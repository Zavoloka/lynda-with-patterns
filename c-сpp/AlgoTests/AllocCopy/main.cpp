#include <iostream>

char* alloc_copy(const char* str){

    char * tempPtr = (char *)str;
    char * newStr =  nullptr;

    int count = 0;
    bool terminationFlag = true;

    // http://www.cplusplus.com/reference/cstdlib/realloc/
    while (terminationFlag){

        // copy the end of the line and exit from the loop
        if (*tempPtr == '\0'){
            terminationFlag = false;
        }

        //std::cout << "original : " <<  *(tempPtr) << std::endl;

        // allocate additional memory for output string
        newStr = (char*)realloc(newStr, count * sizeof(char));

        // copy new element
        *(newStr + count * sizeof(char)) = *tempPtr;

        //std::cout << "new one : " << *(newStr + count * sizeof(char)) << std::endl;
        //std::cout << "prev one# : " << *(newStr + (count-1) * sizeof(char)) << std::endl;

        count++;
        tempPtr++;

    }

    return newStr;
};



void print_char_array(const char* str){

    bool terminationFlag = true;
    while (terminationFlag){
   
        std::cout << *(str) << std::endl;
        str++;

        if (*str == '\0'){
            terminationFlag = false;
        }
    }

}

int main(){

    char const * baseStr = {"Hello there!"};
    char * newStr = alloc_copy(baseStr);

    // test 
    std::cout << "----- test print -----" << std::endl;
    print_char_array(newStr);
 
    return 0;
}