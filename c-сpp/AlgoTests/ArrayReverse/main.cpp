#include <iostream>
#include <sstream>
#include <iterator>
#include <algorithm>


// TODO : make a special classes with overloads
void printArray(int * arrayOfNumbers, size_t size, std::string title){

    // what if amout of numbers is not odd
    std::cout << title << std::endl;
    for (size_t i = 0; i < size; i++){
        std::cout << *(arrayOfNumbers + i) << ", ";
        
    }
    std::cout << std::endl;
}

void printArrayStd(int arrayOfNumber[5], std::string title){

    // what if amout of numbers is not odd
    std::cout << title << std::endl;
    for (size_t i = 0; i < 5; i++){
        std::cout << arrayOfNumber[i] << ", ";
        
    }
    std::cout << std::endl;
}


void reverseArray(int * arrayOfNumbers, size_t size){

    // what if amout of numbers is not odd
    size_t iterations = (size_t)size/2;
    for (size_t i = 0; i < iterations; i++){
        int temp = *(arrayOfNumbers + i);
        *(arrayOfNumbers + i) = *(arrayOfNumbers + size - 1 - i);
        *(arrayOfNumbers + size - 1 - i) = temp;

        std::stringstream printStr;
        printStr << "Reversed array (" << i << "): ";
        printArray(arrayOfNumbers, 9, printStr.str());

    }
}


// in addition https://www.geeksforgeeks.org/program-reverse-array-using-pointers/
// https://stackoverflow.com/questions/13189996/array-element-values-element-addresses-and-pointer-increment


// TODO : Try yo rewrite only pointers without using values
// Finish this one
/*void reverseArrayPointers(int * arrayOfNumbers, size_t size){

    // what if amout of numbers is not odd
    size_t iterations = (size_t)size/2;
    for (size_t i = 0; i < iterations; i++){
        int ** addressHolder = (&arrayOfNumbers) + sizeof(int);
        &(&arrayOfNumbers) + sizeof(int)) = &(arrayOfNumbers + size - 1 - i);
        (arrayOfNumbers + size - 1 - i) = &temp;

        // ** int

        std::stringstream printStr;
        printStr << "Reversed array (" << i << "): ";
        printArray(arrayOfNumbers, 9, printStr.str());

    }
}*/

// https://stackoverflow.com/questions/20059602/can-stdbegin-work-with-array-parameters-and-if-so-how
// https://en.cppreference.com/w/cpp/iterator/begin

// make it tamplate
void reverseArrayStd( int (&arrayOfNumbers)[5]){
    
    
    // uses #<iterator> and <algorithm>  
    std::reverse(std::begin(arrayOfNumbers), std::end(arrayOfNumbers));
}


int main(){


    int * foo = new int[9];
    foo[0] = 23; foo[1] = -3; foo[2] = 43;
    foo[3] = 3; foo[4] = 23; foo[5] = -23;
    foo[6] = 4; foo[7] = 0; foo[8] = -215;

    std::cout << std::endl << "Value reverse[odd] : " << std::endl;
    printArray(foo, 9, "Original array : ");
    reverseArray(foo, 9);
    printArray(foo, 9, "Reversed array : ");

    // TODO : test on pairs
     int * bar = new int[10];
    bar[0] = 23; bar[1] = -3; bar[2] = 43; bar[3] = 3; 
    bar[4] = 23; bar[5] = -23; bar[6] = 4; bar[7] = 0; 
    bar[8] = -215; bar[9] = -67;



    std::cout << std::endl << "Value reverse[even] : " << std::endl;
    printArray(bar, 10, "Original array : ");
    reverseArray(bar, 10);
    printArray(bar, 10, "Reversed array : ");


    int tet[5];
    tet[0] = 11; tet[1] = -22; tet[2] = 33;
    tet[3] = 44; tet[4] = 5;

    std::cout << std::endl << "STD reverse : " << std::endl;
    printArrayStd(tet, "Original tet array[odd] : ");
    reverseArrayStd(tet);
    printArrayStd(tet, "Reversed tet array[odd] : ");

    return 0;

}