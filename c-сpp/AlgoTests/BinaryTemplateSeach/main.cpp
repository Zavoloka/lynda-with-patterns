
#include <iostream>

// https://stackoverflow.com/questions/3175219/restrict-c-template-parameter-to-subclass
// https://stackoverflow.com/questions/3694630/c-const-reference-before-vs-after-type-specifier

/*

template <typename T>
boost::enable_if< boost::is_base_of<Base,T>::value >::type function() {
   // This function will only be considered by the compiler if
   // T actualy derived from Base
}

*/

// Plan : 
// 1. do without additional prechecks

template <typename T>
size_t binarySearch(T * arrayIsHere, std::size_t size, const T & searchElement){

    // pre-checks 
    // size is not null, array is not nullptr, search elemnt is not empty, etc..

    std::size_t leftRange = 0;
    std::size_t rightRange = size-1;
    std::size_t midElementIndex = 0;
    int * currentElementAddress = nullptr;

    do {

        midElementIndex = (std::size_t)(rightRange + leftRange)/2; 

        std::cout << "left : " << leftRange 
                  << " mid element : " << midElementIndex 
                  <<  " right : " << rightRange << std::endl;


        // get address of the current mid elemnt  
        // we need a workaround here to resolve convertion from (const * int) to (* int)
        currentElementAddress = (arrayIsHere + (sizeof(int) * midElementIndex));
        // compare value of current mid element with our  search element
        if (*currentElementAddress > searchElement){
            /// >=

           rightRange = midElementIndex;
           // continue;

            /*
            How continue works in while loop
            ---------------------------------------------------                       
            https://en.cppreference.com/w/cpp/language/continue
            do {
                // ...
                continue; // acts as goto contin;
                // ...
                contin:;
            } while (/* ... *\/);
            */

        } 

        // there is a possibility to stuck in a local minimum 
        if (*currentElementAddress < searchElement){

        leftRange = midElementIndex;
            //continue;
        }


    } while( (*currentElementAddress != searchElement) && ( (rightRange - leftRange) > 1) );

    if ( *currentElementAddress == searchElement){
        return midElementIndex;
    }

    // TODO : decide how to represent a NULL result
    return 0;

}

// TODO : include the test cases 
// with some test framework
// also include boost here

int main(){

   // binarySearch(const T * arrayIsHere, size_t size, T searchElement);
    
    // binary search should be sorted
    size_t fooSize = 9;
    int * foo = new int[fooSize];
    foo[0] = -215; foo[1] = -23; foo[2] = 0;
    foo[3] = 3; foo[4] = 23; foo[5] = 33;
    foo[6] = 54; foo[7] = 120; foo[8] = 324;

    int searchElement = -23;

    std::cout << "Search element : " << searchElement  << std::endl;
    std::size_t elemntIndex = binarySearch<int>(foo, fooSize, searchElement);

    std::cout << std::endl;

    searchElement = 324;
    elemntIndex = binarySearch<int>(foo, fooSize, searchElement);

    // render steps

}