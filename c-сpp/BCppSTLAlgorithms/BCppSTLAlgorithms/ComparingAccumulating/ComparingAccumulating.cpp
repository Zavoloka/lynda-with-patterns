﻿// ComparingAccumulating.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include <numeric>

using namespace std;

int main()
{
    std::cout << "ComparingAccumulating Hello!\n"; 

	// equal(), mismatch()

	vector<int> a{ 1, 2, 3, 5 };
	vector<int> b{ 1, 2, 0, 4 };


	bool isSame = equal(begin(a), end(a), begin(b), end(b));

	if (!isSame)
	{
		auto firstChange = mismatch(begin(a), end(a), begin(b));
		cout << "first mismatch : " << *(firstChange.first) << " " << *(firstChange.second) << endl;

		auto distance = firstChange.first - begin(a);
		cout << "distance : " << distance << endl;
	}
	

	int sumOfVect = accumulate(begin(a), end(a), 0);  
	cout << "Accumulate of a : " << sumOfVect << endl;

	int sumOfVectWithLambda = accumulate(begin(a), end(a), 0, [](int total, int i) { return total + i * 2; });
	cout << "Accumulate of a with lambda x2 : " << sumOfVectWithLambda << endl;

	// For loops

	// for (auto it = begin(a); it != end(a); it++) { ... }
	// for (auto& i:a) { ... }
	// for_each(begin(a), end(b), [](int& elem){ ... });

	// -----------------------------------------------

	// Copy collection

	// operator=
	// loop ~
	// copy()
	// copy_if()
	// copy_n()
	// copy_backward() - helps avoid overlaps

	vector<int> source{ 3, 6, 1, 0, -2, 5 };
	vector<int> v2(source.size());

	copy(begin(source), end(source), begin(v2));

	auto v3 = source;

	auto position = find(begin(source), end(source), 0);
	vector<int> v4(source.size());
	copy(begin(source), position + 1, begin(v4));

	cout << "partial copy source-v4 : " << endl;
	for_each(begin(v4), end(v4), [](int& elem) { cout << "elem:" << elem << endl; });

	//copy_backward(begin(v4), end(v4) - 1, end(source));

	//cout << "copy backward source-v4 : " << endl;
	//for_each(begin(source), end(source), [](int& elem) { cout << "elem:" << elem << endl; });


	// -----------------------------------------------

	// Move & Remove

	// move()
	// move_backward()

	// remove()
	// remove_if()
	// Does not shrink the container or change value past the new end
	// erase()


	auto newEnd = remove(begin(source), end(source), 1); // returns iterator which points to new endl, do not delete actual elements
	int s = source.size();
	int logicalSize = newEnd - begin(source);
	source.erase(newEnd, end(source));

	source = v3; //reset
	source.erase(remove(begin(source), end(source), 1), end(source));  // in 1 line

	// -----------------------------------------------------------------------------

	vector<int> v6(10);
	fill(begin(v6), end(v6), 1);
	fill_n(begin(v6), 6, 2);
	iota(begin(v6), end(v6), 1);

	int index = 10;
	generate(begin(v6), end(v6), [&index]() {return (index *= 2); });
	

	// replace()
	// replace_if()

	// transform() to every element
	// transform(begin(source), end(source) - 1, begin(source) + 1, begin(v6)

    // -----------------------------------------------

    vector<int> v7(5); 
    vector<int> v8(5);
    
    iota(begin(v7), end(v7), 0); 
    fill(begin(v8), end(v8), 0);

    transform(begin(v7), end(v7) - 1, begin(v7)+1, begin(v8), [](int a, int b) { return a * b; });

    // Result
    // v7: 0, 1, 2, 3, 4
    // v8: 0, 2, 6, 12 ,0

    // -----------------------------------------------

    // unique
    // unique_copy

    // Remove consecutive duplicates by shifting them to the end if container
    // unique forks only on sorted arrays
    vector<int> v9 = { 1, 2, 3, 3, 4, 5, 6, 7 };
    vector<int> v10 = { 1, 2, 3, 3, 4, 5, 6, 3 };
    auto v9it = unique(begin(v9), end(v9));
    auto v10it = unique(begin(v10), end(v10)); // returns a past-the-end iterator for the new logical end of the range.
    v10.erase(v10it, end(v10));

    // ...

    // reverse(), iter_swap(), reverse_copy()

	// -----------------------------------------------

	int endnum;
	cin >> endnum;

}