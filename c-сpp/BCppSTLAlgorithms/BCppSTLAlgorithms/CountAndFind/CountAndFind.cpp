﻿// CountAndFind.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
    std::cout << "Hello World!\n"; 


	// ------------------------------------------------------------------
	// ------------------------- СOUNTING -------------------------------
	// ------------------------------------------------------------------
	vector<int> v{ 1, 2, 3, 5, 6 };

	int const targetNumber1 = 4;
	int const targetNumber2 = 5;


	// v.begin() - member functions
	// begin(v)  - non-member functions (checks if it has member function and use it instead)

	const auto targetNumber1Results = count(v.begin(), v.end(), targetNumber1);
	const auto targetNumber2Results = count(begin(v), end(v), targetNumber2);


	cout << "Target number 1 : " << targetNumber1 << " | Count results : " << targetNumber1Results << endl;
	cout << "Target number 2 : " << targetNumber2 << " | Count results : " << targetNumber2Results << endl;

	// ----------------------------------------------------------------------------------------------------


	auto const oddNumbersCount = count_if(begin(v), end(v), [](auto elem) {return elem % 2 == 0; });

	cout << "Target number 3(even) | Count results : " << oddNumbersCount << endl;


	// helper shortcuts

	bool allof, noneof, anyof;
	allof = (oddNumbersCount == v.size());
	noneof = (oddNumbersCount == 0);
	anyof = (oddNumbersCount > 0);


	auto oddLambda = [](auto elem) 
	{
		return elem % 2 == 0;
	};


	bool const allofSTL = all_of(begin(v), end(v), oddLambda);
	bool const noneofSTL = none_of(begin(v), end(v), oddLambda);
	bool const anyofSTL = any_of(begin(v), end(v), oddLambda);
	 

	cout << "all  of example / raw : " << allof  << " |  stl : " << allofSTL << endl;
	cout << "none of example / raw : " << noneof << " |  stl : " << noneofSTL << endl;
	cout << "any  of example / raw : " << anyof  << " |  stl : " << anyofSTL << endl;


	// ------------------------------------------------------------------
	// ------------------------- FINDING --------------------------------
	// ------------------------------------------------------------------

	const auto findResult = find(begin(v), end(v), 3); // return iterator
	cout << "find result " << *findResult << endl;
	
	// find_if_not, find_first_of, search, find_end, search_n, adjacent_find

	// -----------------------------------

	int temp = 0;
	cin >> temp;
}