﻿#include "pch.h"
#include <iostream>

#include <algorithm>
#include <numeric>
#include <vector>

using namespace std;

int main()
{
    // swap, iter_swap, swap_ranges
    vector<int> a = { 1,2,3,4,5 };
    iter_swap(begin(a), begin(a) + 1);

    vector<int> b = { 5,4,3,2,1 };
    swap_ranges(begin(a), find(begin(a), end(a), 3), begin(b));

    // --------------------------------------
    // Rotate
    // https://en.cppreference.com/w/cpp/algorithm/rotate

    vector<int> tasks(6);
    iota(begin(tasks), end(tasks), 1);

    // tasks : 1, |2, 3, 4,| 5, 6
    auto tasksBegin = begin(tasks);
    auto tasksEnd = end(tasks);
    
    auto two = find(tasksBegin, tasksEnd, 2);
    auto four = find(tasksBegin, tasksEnd, 4);
    rotate(two, four, four + 1);  // (begin, middle, end);
    // tasks (result) : 1, |4, 2, 3,| 5 ,6

    // Other case:
    //  rotate(two, four, four + 2);  // (begin, middle, end);
    //  tasks                 : 1,  2, 3, 4, 5,  6
    //  tasks (alter. result) : 1, |4, 5, 2, 3,| 6

    // --------------------------------------
    // Partition, Stable Partition, Rotate and Partition

    vector<int> numbers(8);
    iota(begin(numbers), end(numbers), 1);

    // returns border between selected and unselected items
    // move all even number to the beginning
    auto selected = stable_partition(begin(numbers), end(numbers), [](int i) { return i % 2 != 0; });
   
    // rotate everything before 4
    auto numberFour = find(begin(numbers), end(numbers), 4);
    rotate(begin(numbers), selected, numberFour);
    // rotate(firs, newfirst, last)

    // Additional hints:
    // rotate_copy(first, newfirst, last, destfirst)
    // partial_sort(first, middle, last)
    // inplace_merge(first, middle, last)

    // --------------------------------------
    int endnum;
    cin >> endnum;
}
