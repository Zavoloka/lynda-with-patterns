﻿#include "pch.h"
#include <iostream>
#include <algorithm>

#include <iterator>
#include <numeric>
#include <vector>
#include <deque>
#include <array>

using namespace std;

int main()
{
    // Iterators

    vector<int> v2;
    fill_n(back_inserter(v2), 6, 2); // fills vector with six '2'
    generate_n(back_inserter(v2), 10, [n = 0]() mutable {return n++; }); // add 10 more elements from 0 to 9

    // (fron_inserter) doesn't work with vector, because it has no push_front;
    deque<int> q3;
    fill_n(front_inserter(q3), 6, 2);
    generate_n(front_inserter(q3), 10, [n = 0]() mutable {return n++; });

    // ---------------------------------------------------------- 

    vector<int> v1;
    transform(begin(v2) + 6, end(v2), back_inserter(v1), [](int a) {return a * 2; });
     
    // ---------------------------------------------------------- 

    // Reverse iterators
    // [    ] [   x   ] [x][x][x] [   x  ] [   ]
    //  rend |  begin  |   ...   | rbegin | end

    v2.clear();
    copy(rbegin(v1), rend(v1), back_inserter(v2));

    // same behavior for find()
    // ---------------------------------------------------------- 

    // Const iterators
    // cbegin(), cend(), crbegin(), crend()

    array<const int, 5> ca = { 3,1,6,0,2 };
    auto cit = cbegin(ca);
    cout << "const iterator begin dereference : " << *cit;


    // ---------------------------------------------------------- 
    int endnum;
    cin >> endnum;
}
