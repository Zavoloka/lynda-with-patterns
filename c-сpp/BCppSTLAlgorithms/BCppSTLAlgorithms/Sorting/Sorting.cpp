﻿// Sorting.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "Employee.h"

using namespace std;

int main()
{

    std::cout << "Hello World!\n"; 


	vector<int> v{ 4, 5, -3, 3, 7, 0, 3, -3, -7, -2, 17 };


	sort(begin(v), end(v));
	sort(begin(v), end(v), [](int elem1, int elem2) { return elem1 > elem2; });
	sort(begin(v), end(v), [](int elem1, int elem2) { return abs(elem1) > abs(elem2); });



	vector<Employee> staff
	{
	
		{ "Kate", "Gregory", 1000 },
		{ "Obvious", "Artificial", 2000 },
		{ "Fake", "Name", 2000 },
		{ "Alan", "Turing", 2000 },
		{ "Grace", "Hooper", 2000 },
		{ "Anita", "Borg", 2000 }
	
	};


	sort(begin(staff), end(staff), [](Employee e1, Employee e2) { return e1.getSalary() < e2.getSalary(); });
	sort(begin(staff), end(staff), [](Employee e1, Employee e2) { return e1.getSortingName() < e2.getSortingName(); });

	// Ensure that equal elements will not change order
	stable_sort(begin(staff), end(staff), [](Employee e1, Employee e2) { return e1.getSalary() < e2.getSalary(); });


	const auto sorted = is_sorted(begin(v), end(v), [](int elem1, int elem2) { return elem1 > elem2; });
	// max_element, min_element

	// find()  [ but in sorted collection, you can use a bindary search ]
	// upper_bound(), lower_bound() -> first element after selected
	// max_element(), min_element(); for unsorted
	// largest is last, smallest is first, for sorted


	// ---------------------------------------------------------------------------------------------------------------

	random_device randomdevice;
	mt19937 generator(randomdevice());

	shuffle(begin(v), end(v), generator);
	partial_sort(begin(v), find(begin(v), end(v), -3), end(v));

	// is_sorted_until(), partial_sort_copy()


	std::cout << "v::for_each : " << std::endl;
	std::for_each(begin(v), end(v), [](int &i) {std::cout << " elem: " << i << std::endl; });


	// std::nth_element(first, current, last)
	/*
		Sort element in range
		Rearranges the elements in the range [first,last), 
		in such a way that the element at the nth position is the element that would be in that position in a sorted sequence.

		The other elements are left without any specific order, except that none of the elements 
		preceding nth are greater than it, and none of the elements following it are less.
	*/


}
