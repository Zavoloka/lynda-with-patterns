﻿// Cpp17Features.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <type_traits>
#include <map>
using namespace std;


template<typename T>
auto length(T const& value) {
	if constexpr(is_integral<T>::value)
	{
		// The length of an integer number is the number itself
		return value;
	}
	else
	{
		// Invoke the length method in the general case
		// will fail without constexpr for types which doesn't support .length() method
		return value.length();
	}
}


int main()
{
    std::cout << "Hello World!\n"; 
	

	// C++ 17 if/switch var declaration
	string names[3] { "test1", "test2", "test3" };
	if (const auto it = find(begin(names), end(names), "test2"); it != end(names))
	{
		cout << "it was found";
	}
	else 
	{
		cout << "it wasn't found";
	}



	// ------------------------
	int n{ 64 };
	string s{ "Connie" };

	cout << "\n n = " << n << " / length(n) = " << length(n) << endl;
	cout << "\n s = " << s << " / length(s) = " << length(s) << endl;
	


	// -------------------------


	map<string, string> italianDictionary{};
	auto [position, success] = italianDictionary.insert({"sedia", "chair"});

	cout << "Map position : " << (*position).first << " | " << (*position).second << endl;
	cout << "Map success : " << success << endl;


}

// nested namespaces
//namespace PluralsightEngine::Graphics::Rendering {
//	class OpenGLRenderer;
//}