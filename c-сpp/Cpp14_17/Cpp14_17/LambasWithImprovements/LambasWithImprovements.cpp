﻿// LambasWithImprovements.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
    std::cout << "Hello World!\n"; 
	vector<string> names{ "test", "name", "aa", "bbbb" };

	sort(begin(names), end(names),
		[](string const& a, string const& b) 
			{
				return (a.length() < b.length());
			}
	);
	
	sort(begin(names), end(names),

		// generic (polimorphic) Lambda
		[](auto const& a, auto const& b) {
				return (a.length() < b.length());
			}
	);


	// capture list
	int x{ 1 };
	int y{ 2 };

	unique_ptr<int> p1 = make_unique<int>(7);

	// X is captured by value, Y is captured by ref
	auto a =
		[x, &y, z = 64, p2{ move(p1)}](auto a, auto b) {
		
		// do something
		cout << "Lambda init-captures:" << endl;
		cout << "X: " << x << endl;
		cout << "Y: " << y << endl;
		cout << "Z: " << z << endl;
		cout << "P2: " << *p2 << endl;
	};

	a(5,7);


		


}

