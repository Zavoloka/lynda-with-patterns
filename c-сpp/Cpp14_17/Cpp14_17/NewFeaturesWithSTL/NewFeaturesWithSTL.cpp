﻿// NewFeaturesWithSTL.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;


// executes during compilation
constexpr int Fibonacci(int n) {

	switch (n) {

	case 0: return 0;
	
	case 1: return 1;

	default :
		return Fibonacci(n - 1) + Fibonacci(n - 2);

	}
}

// https://stackoverflow.com/a/55007814
// SDL checks transforms warnings into errors

//[[deprecated("Too old")]]
void TestDeprecatedFunction() {

	return;
}

template <typename T>
constexpr T pi = T(3.14159265359)

template <typename T>
constexpr T maxValue = T(1000);

// probably works with Clang
//template <>
//constexpr double maxValue<double> = 2000;

//template <>
//constexpr char maxValue = 'Z';



int main()
{

	// 100'000'00  - digit separator

	static_assert(Fibonacci(10) == 55, "Unexpected Fibonacci number");

	// -----------------------------------------------------------------

	TestDeprecatedFunction();



    std::cout << "Hello World!\n"; 
}