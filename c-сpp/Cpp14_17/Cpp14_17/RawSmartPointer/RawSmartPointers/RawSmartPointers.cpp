﻿// RawSmartPointers.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <memory>
#include <chrono>
#include <tuple>

using namespace std;
using namespace std::chrono;


std::tuple<double, char, std::string> get_student(int id)
{
	if (id == 0) return std::make_tuple(3.8, 'A', "Lisa Simpson");
	if (id == 1) return std::make_tuple(2.9, 'C', "Milhouse Van Houten");
	if (id == 2) return std::make_tuple(1.7, 'D', "Ralph Wiggum");
	throw std::invalid_argument("id");
}

int main()
{

	// Smart pointers

	{
		auto p = std::make_unique<double[]>(100000);

		// Example of Factory implementation 
		/*	unique_ptr<SomeObject> MakeObject() {

		}*/

		// 'p' gets destroyed here
	}

	// Chrono

	std::chrono::seconds s{};  // 0 seconds (seconds s{})
	//std::chrono::seconds a;	  // uninitialized (seconds s)

	auto b = 10s;
	cout << s.count() << endl;

	auto y = 150ms;
	cout << y.count() << endl;

	constexpr auto w = 3s;
	cout << w.count() << endl;

	// Tuple

	auto tupleExample = get_student(1);
	//						2.9										2.92
	cout << "Tuple - 1 : " << std::get<0>(tupleExample) << " == " << std::get<double>(tupleExample);

	float f;
	cin >> f;


}

