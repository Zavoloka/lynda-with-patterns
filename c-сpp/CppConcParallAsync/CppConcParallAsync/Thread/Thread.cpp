﻿// Thread.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <thread>
#include <atomic>

using namespace std;

atomic<long> multiThreadedSum{ 0 };

void SumNumbers(const vector<int>& toBeSummed, int idxStart, int idxEnd)
{
	for (int i = idxStart; i <= idxEnd; ++i)
	{
		multiThreadedSum += toBeSummed[i];
	}

}


int main()
{
    std::cout << "Hello World!\n"; 

	vector<int> toBeSummed;
	for (int i = 0; i < 30000; ++i)
	{
		toBeSummed.push_back(rand());
	}

	thread t1(SumNumbers, toBeSummed,	  0,  9999);
	thread t2(SumNumbers, toBeSummed, 10000, 19999);
	thread t3(SumNumbers, toBeSummed, 20000, 29999);

	t1.join();
	t2.join();
	t3.join();

	
	printf("The sum was %d\n", multiThreadedSum.load());
}

