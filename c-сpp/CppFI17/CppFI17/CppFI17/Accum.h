#pragma once
template <class T>
class Accum
{
private:

	T total;

public:

	Accum();
	Accum(T start) : total(start) {};

	T Max(T const & t1, T const & t2) { return t1 < t 2 ? t2 : t1; };
	T operator+=(T const & t) { return total = total + t; };
	T GetTotal() const { return total; }

};
