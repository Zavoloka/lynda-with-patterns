﻿// IndirectionAndMemoryManagement.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include "ConstClass.h"
#include "ResourceDemo.h"
#include "RawResourceManager.h"
#include "SmartResourceManager.h"

using std::string;

int main()
{

	// Indirection, const, Memory Management

	int i = 3;
	int const ci = 3;
	//ci = 5; - error

	int const & cri = i;

	ConstClass cc;
	
	std::cout << " ConstClass -> SetA(), GetA() " << std::endl;
	int aa = 5;
	cc.SetA(aa);
	std::cout << cc.GetA() << std::endl;


	// const indirection

	int const * pcA = new int(3); // value can't be changed
	int * const pcB = new int(4); // address can't be changed
	int const * const pcC = new int(5); // can't change anything 
	
	pcA = pcB;		 // can change address
	//*(pcA) = 4;    // error
	//pcB = pcA;     // error
	*(pcB) = *pcA;   // can change value
	//pcC = pcB;	 // error
	//*(pcC) = *pcB; // error


	// -------------------------- memory examples --------------

	// local scope, allocation on stack
	{
		ResourceDemo localResource("local");
		string localString = localResource.GetName();

	} // end of scope, localResource calls destructor

	// allocation raw pointer on heap
	ResourceDemo* pResource = new ResourceDemo("created with new");
	string newString = pResource->GetName();

	/* bad example which shows possible memory leak

	int j = 3;
	if (j === 3) {

		return 0; // destructor wasn't called for pResource;
	}
	*/
	ResourceDemo* pResource2 = pResource;
	delete pResource;
	pResource = nullptr; // good practice but still willn't help

	//string string3 = pResource->GetName(); //exception
	//pResource2; // oops, pResource is not accessible any more


	{

		RawResourceManager rrm;
		rrm.AddResource();
		//rrm.AddResource(); // error prone, memory leak

		// RawResourceManager rrm2 = rrm; // error prone without a proper copy assignment
		// will call an additional destructor for rrm2 which is linked to rrm

		RawResourceManager rrm2 = rrm; // with copy assignment
	}


	{
		SmartResourceManager srm;
		srm.AddResource();
		srm.AddResource();		// everything is ok


		SmartResourceManager srm2 = srm;	// still ok
	}



	int a;
    std::cout << "Hello World!\n"; 
	std::cin >> a;



}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
