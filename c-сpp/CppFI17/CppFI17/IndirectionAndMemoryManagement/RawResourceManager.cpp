#include "pch.h"
#include "RawResourceManager.h"
#include "ResourceDemo.h"


RawResourceManager::RawResourceManager():pResource(nullptr)
{
}

void RawResourceManager::AddResource() {

	// during the second call the prev. address will be erised
	pResource = new ResourceDemo("Raw Resource for init");
}

RawResourceManager::~RawResourceManager()
{
	delete pResource;
}

RawResourceManager::RawResourceManager(RawResourceManager const & rrm)
{
	pResource = new ResourceDemo(rrm.pResource->GetName());
}

RawResourceManager & RawResourceManager::operator=(const RawResourceManager & rrm)
{
	
	// doesn't check for self assignment
	// if (&rrm == this) return;
	
	delete pResource;
	pResource = new ResourceDemo(rrm.pResource->GetName());
	return *this;
	
}


