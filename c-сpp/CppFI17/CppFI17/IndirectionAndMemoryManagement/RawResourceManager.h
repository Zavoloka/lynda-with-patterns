#pragma once
#include "ResourceDemo.h"
class RawResourceManager
{

	ResourceDemo* pResource;

public:
	RawResourceManager();
	RawResourceManager(RawResourceManager const & rrm);				// copy constructor
	RawResourceManager& operator = (const RawResourceManager& rrm); // copy assignment

	void AddResource();

	~RawResourceManager();
};

