#include "pch.h"
#include "ResourceDemo.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;


ResourceDemo::ResourceDemo(std::string n):name(n)
{
	cout << "constructing " << name << endl;
}


ResourceDemo::~ResourceDemo()
{

	cout << "destructing " << name << endl;
}
