#pragma once
#include <string>

class ResourceDemo
{
private:
	std::string name;
public:
	ResourceDemo(std::string n);
	~ResourceDemo();
	std::string GetName() const { return name; }
};

