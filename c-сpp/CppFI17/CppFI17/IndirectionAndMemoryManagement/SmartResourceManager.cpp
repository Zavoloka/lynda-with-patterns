#include "pch.h"
#include "SmartResourceManager.h"


SmartResourceManager::SmartResourceManager()
{


}


SmartResourceManager::SmartResourceManager(SmartResourceManager const & srm)
{

	pResource = srm.pResource;
}

SmartResourceManager& SmartResourceManager::operator=(const SmartResourceManager& srm)
{
	pResource = srm.pResource;
	return *this;
}

void SmartResourceManager::AddResource()
{

	pResource.reset();
	pResource = std::make_shared<ResourceDemo>("Resource for smart pointer");
}


