#pragma once
#include "ResourceDemo.h"
#include <memory>

class SmartResourceManager
{

	// https://stackoverflow.com/questions/20895648/difference-in-make-shared-and-normal-shared-ptr-in-c
	std::shared_ptr<ResourceDemo> pResource;

public:
	SmartResourceManager();
	SmartResourceManager(SmartResourceManager const & rrm);				// copy constructor
	SmartResourceManager& operator= (const SmartResourceManager& srm); // copy assignment

	void AddResource();



};

