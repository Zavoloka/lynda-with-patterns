#pragma once
#include <string>

using namespace std;

class Person
{

private:

	string firstname;
	string lastname; 


public:
	Person(string firstname, string lastname);
	~Person();


	string GetName() const;
};

