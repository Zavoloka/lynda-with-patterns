#pragma once


template <class T>
class Accum
{
private:

	T total;

public:

	Accum() : total(0) {};
	Accum(T start) : total(start) {};

	// forbids copy constructor
	Accum(const Accum &) = delete;

	// forbids move constructor
	Accum(Accum &&) = delete;


	T Max(T const & t1, T const & t2) { return t1 < t2 ? t2 : t1; };
	T operator+=(T const & t) { return total = total + t; };
	T GetTotal() const { return total; };

};