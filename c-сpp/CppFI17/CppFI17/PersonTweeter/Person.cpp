#include "Person.h"
#include <iostream>

using namespace std;

Person::Person(string first, string last, int arbitrary) : _firstName(first), _lastName(last), _arbitraryNumber(arbitrary)
{
	cout << "constructing " << GetName() << endl;
}

Person::~Person()
{
	cout << "destructing " << GetName() << endl;
}

string Person::GetName() const
{
	return _firstName + " " + _lastName;
}