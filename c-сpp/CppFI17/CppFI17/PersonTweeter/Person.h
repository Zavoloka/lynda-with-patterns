#pragma once

#include <string>

class Person 
{
private:
	std::string _firstName;
	std::string _lastName;
	int _arbitraryNumber;

public:
	Person(std::string first, std::string last, int arbitrary);
	Person() = default;

	~Person();
	std::string GetName() const;
	int GetNumber() const { return _arbitraryNumber; }
	void SetNumber(int number) { _arbitraryNumber = number; }


};