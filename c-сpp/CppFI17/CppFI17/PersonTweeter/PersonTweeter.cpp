// PersonTweeter.cpp : Defines the entry point for the console application.
//
#include "Person.h"
#include "Tweeter.h"
#include "status.h"
#include "Accum.h"

#include <iostream>
//#include <string>

using std::cout;
using std::cin;
using std::endl;

int main()
{


	cout << "Flow control and scope demo" << endl;
	Person p1("Kate", "Gregory", 123);
	{
		Tweeter t1("Someone", "Else", 456, "@whoever");
		// tweeter gets destructed here
	}
	{
		Person p2;
		// p2 gets destructed here
	}
	cout << "After innermost block" << endl;
	cout << "p1: " << p1.GetName() << " " << p1.GetNumber() << endl;
	p1.SetNumber(2);
	cout << "p1: " << p1.GetName() << " " << p1.GetNumber() << endl;


	// --------------------------------------

	cout << "Template accumulate/max demo(int)";

	Accum<int> accDemo(5); // or just /**/;
	cout <<  "Max (325, 213) :" << accDemo.Max(325, 213) << endl;


	cout << "Template accumulate/max demo (float)";
	Accum<float> accDemoFloat;
	accDemoFloat += 2;
	accDemoFloat += 4;
	accDemoFloat += 8;
	float totalResult = accDemoFloat.GetTotal();

	cout << "Accumulate (+=) : " << std::to_string(totalResult) << endl;


	//---------------------------------------

	cout << "Reference, pointer demo : ";

	// original John
	Person John("John", "Doe", 55);
	
	// reference John
	Person& rJohn = John;
	rJohn.SetNumber(33);
	cout << "John, rJohn (should be 33) : " << John.GetNumber() << endl;


	/*
	- A pointer can be re-assigned while reference cannot, and must be assigned at initialization only.
	- Pointer can be assigned NULL directly, whereas reference cannot.
	- Pointers can iterate over an array, we can use ++ to go to the next item that a pointer is pointing to.
	- A pointer is a variable that holds a memory address. A reference has the same memory address as the item it references.
	- A pointer to a class/struct uses �->'(arrow operator) to access it�s members whereas a reference uses a �.'(dot operator)
	- A pointer needs to be dereferenced with * to access the memory location it points to, whereas a reference can be used directly.
	
	*/

	Person * pJohn = &John;
	Person * prJohn = &rJohn;

	// access to method through the pointer via ->
	prJohn->SetNumber(11);
	cout << "John, pJohn (should be 11) : " << John.GetNumber() << endl;

	// access to method through the pointer from reference via ->
	prJohn->SetNumber(44);
	cout << "John, pJohn (should be 44) : " << John.GetNumber() << endl;

	// access to method through the value by pointer with (.)
	(*prJohn).SetNumber(77);
	cout << "John, pJohn (should be 77) : " << John.GetNumber() << endl;


	/*
		tl;dr
	
		!If you got a pointer, from new, you have to keep track of it. 
		- At some point you must call `delete` 

		What happens if someone copies it?
		What happens if the local variable (the pointer) goes out of scope early?
		Common mistakes : 
			- Delete too soon
			- Delete twice
			- Never delete 

		-------------------------------------------------------------------------


		Rule of Three 

		- Destructor (Deletes what may have been created with new)
		- Copy constructor (Uses new to initialize from existing value)				| class_name(const class_name &)
		- Copy assignment operator (Deletes, then uses new to initialize)			| class_name & operator=( class_name )

		Growth to rule of five

		- Move constructor
		- Move assignment operator


		// rule of Zero is better : )

		shared_ptr - reference counter
		weak_ptr - at a shared_ptr without bumping the reference count
		unique_ptr - noncopyable


	*/


	// Inheritance demo
	// ---------------------------------------------------------------

	Person Bob("Bob", "Ded", 2);
	Person& rpBob = Bob;
	Person* ppBob = &Bob;

	Tweeter BobTweet("BobT", "DedT", 3, "@handler");
	
	Person& rpBobTweet = BobTweet;
	Person* ppBobTweet = &BobTweet;
	Tweeter& rtBobTweet = BobTweet;

	cout << rpBob.GetName() << endl;			
	cout << ppBob->GetName() << endl;
	cout << BobTweet.GetName() << endl;
	cout << rpBobTweet.GetName() << endl;	
	cout << rtBobTweet.GetName() << endl;
	cout << ppBobTweet->GetName() << endl;  

	// virtual methods always runs a child's version

	Person* SomeoneTest = new Tweeter("Someone", "Else", 567, "@handle");
	cout << SomeoneTest->GetName() << endl;
	delete SomeoneTest; // Tweeter destructor never runs because it's not virtual !
	// as soon as you have at least 1 virtual method -> mark destructor as virtual
	// derived class goes first, than parent


	// TODO : Slicing & Casting



	// --------------------------------------
	int temp, temp2;
	cin >> temp >> temp2;

	return 0;

}

