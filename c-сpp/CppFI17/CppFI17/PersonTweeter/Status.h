#pragma once

// "Unscoped enumeration"
enum Status
{
	Pending,
	Approved,
	Cancelled
};


// https://en.cppreference.com/w/cpp/language/enum
// https://stackoverflow.com/questions/32953650/difference-between-enum-and-enum-class
// Have their own scope  and don't pollute the namespace that they are in "Scoped enumerations"
enum class FileError 
{
	Notfound,
	Ok
};

enum class NetwrokError
{
	Disconnected,
	Ok,
};