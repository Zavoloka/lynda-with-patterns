#include "Tweeter.h"
#include <iostream>

using namespace std;


Tweeter::Tweeter(std::string first, std::string last, int arbitrary, std::string handler) : Person(first, last, arbitrary), _tweeterHandler(handler)
{

	cout << "constructing tweeter " << _tweeterHandler << endl;
}

Tweeter::~Tweeter(void)
{
	cout << "destructing tweeter" << _tweeterHandler << endl;

}

std::string Tweeter::GetName() const
{
	// call parent method
	return Person::GetName();
}
