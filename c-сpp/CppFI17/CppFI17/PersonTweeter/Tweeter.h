#pragma once

#include "Person.h"
#include <string>

class Tweeter : public Person
{

private:
	std::string _tweeterHandler;

public:
	Tweeter(std::string first, std::string last, int arbitrary, std::string handler);
	~Tweeter(void);

	std::string GetName() const;
};