﻿#pragma once
#include "DrinkFactory.h"

int main()
{
	DrinkFactory df;
	auto d = df.make_drink("tea", 200);

	DrinkWithVolumeFactory dfv;
	auto d2 = dfv.make_drink("tea", 300);

	getchar();
	return 0;
}