#pragma once
#include <map>
#include <functional>
#include "HotDrinkFactory.h"

struct HotDrink;
struct HotDrinkFactory;

class DrinkFactory
{
	std::map<std::string, std::unique_ptr<HotDrinkFactory>> factories;

public:

	// not lazy
	DrinkFactory()
	{
		factories["coffee"] = std::make_unique<CoffeeFactory>();
		factories["tea"] = std::make_unique<TeaFactory>();
	}

	std::unique_ptr<HotDrink> make_drink(const std::string& name, int value)
	{
		auto drink = factories[name]->make();
		drink->prepare(value);
		return drink;
	}
};

// Functional factory
class DrinkWithVolumeFactory
{
	std::map <std::string, std::function<std::unique_ptr<HotDrink>(int)>> factories;

public:
	
	DrinkWithVolumeFactory()
	{
		factories["tea"] = [](int value) {
			auto tea = std::make_unique<Tea>();
			tea->prepare(value);
			return tea;
		};
	};

	std::unique_ptr<HotDrink> make_drink(const std::string& name, int value)
	{
		return factories[name](value);
	}
};