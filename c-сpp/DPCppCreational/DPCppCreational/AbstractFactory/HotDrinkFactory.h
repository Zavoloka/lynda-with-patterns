#pragma once
#include <memory>
#include "HotDrink.h"

struct HotDrinkFactory
{
	virtual std::unique_ptr<HotDrink> make() = 0;
};

// Implementations
struct CoffeeFactory : HotDrinkFactory
{
	std::unique_ptr<HotDrink> make() override
	{
		return std::make_unique<Coffee>();
	}
};

struct TeaFactory : HotDrinkFactory
{
	std::unique_ptr<HotDrink> make() override
	{
		return std::make_unique<Tea>();
	}
};