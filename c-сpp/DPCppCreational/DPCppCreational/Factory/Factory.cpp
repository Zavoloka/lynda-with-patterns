﻿// Factory.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>

using namespace std;

enum class PointType
{
	cartesian,
	polar
};

class Point
{
public:
	float x, y;

	// problem
	Point(const float x, const float y) :x{ x }, y{ y }
	{
	}

	/* error here
	Point(const float r, const float theta)
	{
	}
	*/

	// another problem, lacking of clarification, and extra unnecessary params
	Point(const float a, const float b, PointType type = PointType::cartesian)
	{
		if (type == PointType::cartesian)
		{
			x = a;
			y = b;
		}
		else
		{
			x = a * cos(b);
			y = a * sin(b);
		}
	}

};

// ----------------------------------------------

// solution
class Point2
{
	Point2(const float x, const float y) : x{ x }, y{ y }
	{
	}

public:

	float x, y;

	// Factory method
	static Point2 NewCartesian(const float x, const float y)
	{
		return { x, y };
	}

	// Factory method
	static Point2 NewPolar(const float r, const float theta)
	{
		return { r*cos(theta), r*sin(theta) };
	}

	friend ostream& operator<<(ostream& os, const Point2& obj)
	{
		return os
			<< "x: " << obj.x
			<< " y: " << obj.y;
	}


	friend class PointFactory;
};


// Factory class
class PointFactory
{
public:
	static Point2 NewCartesian(const float x, const float y)
	{
		return { x, y };
	}

	static Point2 NewPolar(const float r, const float theta)
	{
		return { r*cos(theta), r*sin(theta) };
	}

}; 

// ----------------------------------------------

class Point3
{
	Point3(const float x, const float y) : x{ x }, y{ y }
	{
	}

	// Factory class
	class PointFactory3
	{
	public:
		static Point3 NewCartesian(const float x, const float y)
		{
			return { x, y };
		}

		static Point3 NewPolar(const float r, const float theta)
		{
			return { r*cos(theta), r*sin(theta) };
		}
	};

public:

	float x, y;


	friend ostream& operator<<(ostream& os, const Point3& obj)
	{
		return os
			<< "x: " << obj.x
			<< " y: " << obj.y;
	}


	// Inner factory
	static PointFactory3 Factory;
};


// ---------------------

int main()
{
	auto c1 = Point2::NewCartesian(1, 2);
	cout << c1 << endl;

	auto p1 = Point2::NewPolar(5, 3.14f / 4);
	cout << p1 << endl;


	auto c2 = PointFactory::NewCartesian(1, 2);
	cout << c2 << endl;

	auto p2 = PointFactory::NewPolar(5, 3.14f / 4);
	cout << p2 << endl;


	// ?
	auto p3 = Point3::Factory.NewCartesian(1, 2);
	cout << p3 << endl;

	getchar();
	return 0;
}