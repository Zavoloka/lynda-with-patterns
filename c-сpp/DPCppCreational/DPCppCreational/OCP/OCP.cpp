﻿// OCP.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>

using namespace std;

enum class Color {Red, Green, Blue};
enum class Size {Small, Medium, Large};


struct Product
{
	string name;
	Color color; 
	Size size;
};

typedef vector<Product*> Items;

struct ProductFilter
{

	// Bad of OCP
	static Items by_color(Items items, Color color)
	{
		Items results;
		for (auto& i : items)
		{
			if (i->color == color)
			{
				results.push_back(i);
			}
		}
		return results;
	}

	// Bad of OCP
	static Items by_size(Items items, Size size)
	{
		Items results;
		for (auto& i : items)
		{
			if (i->size == size)
			{
				results.push_back(i);
			}
		}
		return results;
	}

	// Bad of OCP
	static Items by_color_and_size(Items items, Color color, Size size)
	{
		Items results;
		for (auto& i : items)
		{
			if ((i->size == size) && (i->color == color))
			{
				results.push_back(i);
			}
		}
		return results;
	}


	// Specification pattern
	// https://enterprisecraftsmanship.com/posts/specification-pattern-c-implementation/

};


// Fix OCP
template <typename T>
struct ISpecification
{
	virtual bool is_satisfied(T* item) const = 0;
};

template <typename T>
struct IFilter
{
	virtual vector<T*> filter(const vector<T*> items, const ISpecification<T>& spec) const = 0;
};

struct BetterFilter : IFilter<Product>
{
	vector<Product *> filter(const vector<Product *> items, const ISpecification<Product>& spec) const override
	{
		Items results;
		for (auto& p : items)
		{
			if (spec.is_satisfied(p))
			{
				results.push_back(p);
			}
		}
		return results;
	}
};

struct ColorSpecification : ISpecification<Product>
{
	Color color;

	explicit ColorSpecification(const Color color) : color{ color }
	{
	}

	bool is_satisfied(Product* item) const override
	{
		return item->color == color;
	}
};

struct SizeSpecification : ISpecification<Product>
{
	Size size;

	explicit SizeSpecification(const Size size) : size{ size }
	{
	}

	bool is_satisfied(Product* item) const override
	{
		return item->size == size;
	}
};

template <typename T>
struct AndSpecification : ISpecification<T>
{
	const ISpecification<T>& first;
	const ISpecification<T>& second;

	AndSpecification(const ISpecification<T>& first, const ISpecification<T>& second): first{first}, second{second}
	{
	}

	bool is_satisfied(T* item) const override
	{
		return first.is_satisfied(item) && second.is_satisfied(item);
	}

};

// -------------

int main()
{
    
	Product apple{ "Apple", Color::Green, Size::Small };
	Product tree{ "Tree", Color::Green, Size::Large };
	Product house{ "House", Color::Blue, Size::Large };


	// start of Usage
	vector<Product*> all{ &apple, &tree, &house };

	BetterFilter bf;
	ColorSpecification green{ Color::Green };
	SizeSpecification large{ Size::Large };

	auto green_things = bf.filter(all, green);
	for (auto& x : green_things)
	{
		cout << x->name << " is green" << endl;
	}
	// end of Usage #1

	AndSpecification<Product> greenAndLarge{ green, large };
	auto green_and_large_things = bf.filter(all, greenAndLarge);
	for (auto& x : green_and_large_things)
	{
		cout << x->name << " is green and large" << endl;
	}
	// end of Usage #2


	getchar();
	return 0;
}