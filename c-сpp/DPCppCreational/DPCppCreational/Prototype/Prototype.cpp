﻿// Prototype.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>
#include <vector>
#include <string>

#include "Serialization.cpp"

using namespace std;
 
struct Address
{
	string street;
	string city;
	int suite;

	friend ostream& operator<<(ostream& os, const Address& obj)
	{
		return os
			<< "street: " << obj.street << "\n"
			<< "city: " << obj.city << "\n"
			<< "suite: " << obj.suite;
	}
};

struct Contact
{
	string name;
	Address* workAddress = nullptr;

	Contact(const string& name) : name{ name } {};
	Contact(const string& name, Address* const workAddress) : name{ name }, workAddress{ new Address{*workAddress } }{};
	Contact(const Contact& other) :name{ other.name }, workAddress{ new Address{*other.workAddress} }{};

	friend ostream& operator<<(ostream& os, const Contact& obj)
	{
		return os
			<< "name: " << obj.name << "\n"
			<< "workAddress: " << *obj.workAddress;
	}

	~Contact()
	{
		delete workAddress;
	}
};

struct EmployeeFactory
{
	// static unique_ptr<Contact> NewMainOfficeEmployee(string name, int suite) // also possible
	static unique_ptr<Contact> NewMainOfficeEmployee()
	{
		static Address* mainOfficeAddress = new Address{ "123 East DrDrDr", "Oxford", 0 };
		static Contact emploee{ "", mainOfficeAddress};
		return make_unique<Contact>(emploee);
	}
};


struct EmployeeFactory2
{
public :

	static unique_ptr<Contact> NewMainOfficeEmployee(string name, int suite)
	{
		// Repetitive code, demo only, could be structured better
		static Address* mainAddress = new Address{ "123a", "Cambridge", 0 };
		auto contact = NewEmployee(name);
		contact->workAddress = new Address{ *mainAddress };
		contact->workAddress->suite = suite;

		return contact;
	};

	static unique_ptr<Contact> NewAuxOfficeEmployee(string name, int suite)
	{
		static Address* auxAddress = new Address{ "123b", "Cambridge", 0 };
		auto contact = NewEmployee(name);
		contact->workAddress = new Address{ *auxAddress };
		contact->workAddress->suite = suite;

		return contact;
	};

private :
	static unique_ptr<Contact> NewEmployee(string name)
	{
		auto result = make_unique<Contact>(name);
		return result;
	}
};




int main()
{
	Address* addr = new Address{ "123 East Dr", "London" }; //prototype (base for deep copy)

	Contact john{ "John Doe", addr };
	john.workAddress->suite = 100;
	Contact jane{ "Jane Doe", addr };
	jane.workAddress->suite = 123;

	delete addr;

	cout << john << endl << jane << endl;
	cout << "\n----------------\n" << endl;
	Contact jill{ jane };
	jill.name = "Jill Jill";
	jill.workAddress->suite = 100;

	cout << john << endl << jane << endl << jill << endl;
	cout << "\n----------------\n" << endl;

	// -----------------------------------------------------
	// Prototype

	// memory leak
	//Contact emploee{ "", new Address{"123 East DrDrDr", "Oxford", 0} };
	//Contact john2{ emploee };

	auto john2 = EmployeeFactory::NewMainOfficeEmployee();
	john2->name = "John2";
	john2->workAddress->suite = 11;

	cout << *john2 << endl;
	cout << "\n----------------\n" << endl;

	// -----------------------------------------------------

	auto john3 = EmployeeFactory2::NewAuxOfficeEmployee("John3", 3);
	john3->name = "John3";
	john3->workAddress->suite = 12;

	auto john4 = EmployeeFactory2::NewMainOfficeEmployee("John4", 3);
	john4->name = "John4";
	john4->workAddress->suite = 13;

	cout << *john3 << endl << *john4 << endl;


	// -----------------------------------------------------
	// Deep copy via serialization

	ContactBoost johnB;
	johnB.name = "John Boost Doe";
	johnB.address = AddressBoost{ "123 East Dr", "London", 123 };
	
	// https://theboostcpplibraries.com/boost.serialization-archive

	auto clone = [](ContactBoost c)
	{
		ostringstream oss;
		boost::archive::text_oarchive oa(oss);
		oa << c;

		string s = oss.str();
		ContactBoost result;
		istringstream iss(s);
		boost::archive::text_iarchive ia(iss);

		ia >> result;
;		return result;
	};

	ContactBoost janeB = clone(johnB);
	janeB.name = "Jane";
	janeB.address.street = "123B West Dr";

	cout << "serialization JaneB " << janeB << endl;

	getchar();
	return 0;
}