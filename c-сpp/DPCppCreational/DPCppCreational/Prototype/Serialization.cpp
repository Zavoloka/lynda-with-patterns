
#include <string>
#include <iostream>
#include <memory>
#include <functional>
#include <sstream>

#include <boost/serialization/serialization.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

using namespace std;

// boost include instruction 
// https://www.boost.org/doc/libs/1_65_0/more/getting_started/windows.html#header-only-libraries
// https://stackoverflow.com/questions/45974108/how-to-add-boost-library-1-65-or-1-64-to-visual-studio-2017-project
// https://stackoverflow.com/questions/20623223/boost-not-seeing-jamfile-in-current-directory
// https://www.boost.org/doc/libs/1_62_0/more/getting_started/windows.html#simplified-build-from-source
// https://boostorg.github.io/build/tutorial.html
// https://stackoverflow.com/questions/41464356/build-boost-with-msvc-14-1-vs2017-rc

/*
The following directory should be added to compiler include paths:

	I:\Code\libs\boost_1_73_0

The following directory should be added to linker library paths:

	I:\Code\libs\boost_1_73_0\stage\lib
*/

struct AddressBoost
{
	string street, city;
	int suite;

	AddressBoost() = default;
	AddressBoost(string street, string city, int suite) :street(street), city(city), suite(suite) {};

	friend std::ostream& operator<<(std::ostream& os, const AddressBoost& obj)
	{
		return os
			<< "street: " << obj.street
			<< " city: " << obj.city
			<< " suite: " << obj.suite;
	};

private: 
	friend class boost::serialization::access;

	template<class Ar>
	void serialize(Ar& ar, const unsigned int version)
	{
		ar & street;
		ar & city;
		ar & suite;
	}
};

struct ContactBoost
{
	string name;
	AddressBoost address;

	ContactBoost() = default;

	friend std::ostream& operator<<(std::ostream& os, const ContactBoost& obj)
	{
		return os
			<< "name: " << obj.name
			<< " address: " << obj.address;
	}

private:

	template<class Ar>
	void serialize(Ar& ar, const unsigned int version)
	{
		ar & name;
		ar & address;
	}
};