﻿// SRP.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#pragma once
#include <iostream>

#include <string>
#include <vector>
#include <fstream>

using namespace std;

struct Journal
{

	string title;
	vector<string> entries;

	explicit Journal(const string& title) : title{ title }
	{
	}

	void add(const string& entry)
	{
		entries.push_back(entry);
	}


	// bad for SRP, REMOVE
	void save(const string& filename)
	{
		ofstream ofs(filename);
		for (auto& s : entries)
		{
			ofs << s << endl;
		}
	}


};  

// fix SRP
struct PersistenceManager
{
	static void save(const Journal& j, const string& filename)
	{
		ofstream ofs(filename);
		for (auto& s : j.entries)
		{
			ofs << s << endl;
		}
	}
};


// --------------------------

int main()
{
    std::cout << "Hello World!\n";
}