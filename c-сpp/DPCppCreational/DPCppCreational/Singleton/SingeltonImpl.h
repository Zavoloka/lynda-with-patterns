#pragma once
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <string>

//#TODO: Google unittests 


class DataBase
{

protected:
    std::map<std::string, int> m_Capitals;

public:
    virtual int GetPopulation(const std::string& name) = 0;
};

class DummyDatabase : public DataBase
{

public:
    DummyDatabase()
    {
        m_Capitals["alpha"] = 1;
        m_Capitals["beta"] = 2;
        m_Capitals["gamma"] = 3;
    }

    virtual int GetPopulation(const std::string& name) override
    {
        return m_Capitals[name];
    }

};

class SingeltonImpl : public DataBase
{

private:
    SingeltonImpl()
    {

        std::cout << "Init singletone DB init" << std::endl;
        std::ifstream ifs("capitals.txt");
        if (ifs.is_open())
        {
            std::string s, s2;
            while (std::getline(ifs, s))
            {
                getline(ifs, s2);

                // #TODO: includ Boost
                //int popLine = boost::lexical_cast<int>(s2);
                //capitals[s] = popLine;
               
                int popLine = std::stoi(s2);
                m_Capitals[s] = popLine;
            }
        }
        InstanceCount++;
    }


public:
    int InstanceCount = 0;

public: 

    SingeltonImpl(SingeltonImpl const&) = delete;
    void operator=(SingeltonImpl const&) = delete;

    
    static SingeltonImpl& GetInstance()
    {
        static SingeltonImpl db;
        return db;
    }


    virtual int GetPopulation(const std::string& name) override
    {
        return m_Capitals[name];
    }

    /* // Naive implementation
    static SingeltonImpl* GetInstance()
    {
        static SingeltonImpl* m_Instance;
        // not thread safety  
        if (m_Instance == nullptr)
        {
            m_Instance = new SingeltonImpl;
        }

        //return m_Instance;
        return nullptr;
    }*/

};

struct SingeltonRecordFinder
{
    int GetTotalPopulation(std::vector<std::string>& names)
    {
        int result = 0;
        for (const auto& name : names)
        {
            result += SingeltonImpl::GetInstance().GetPopulation(name);
        }
        return result;
    }
};

struct ConfigurableRecordFinder
{
    DataBase& db;
    explicit ConfigurableRecordFinder(DataBase& db) : db{db}{}

    int GetTotalPopulation(const std::vector<std::string>& names)
    {
        int result = 0;
        for (const auto& name : names)
        {
            result += db.GetPopulation(name);
        }
        return result;
    }

};