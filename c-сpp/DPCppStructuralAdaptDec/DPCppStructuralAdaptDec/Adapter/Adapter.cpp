﻿// Adapter.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stack>
#include <vector>
//#include <boost/algorithm/string.hpp>
#include <iostream>

using namespace std;

class StringAdapter
{
    string s;

public:
    StringAdapter(const string& cs) : s{ cs }
    {
    }

    StringAdapter to_lower() const
    {
        string ss{ s };
        //boost::to_lower(ss);
        return { ss };
    }


    vector<string> split(const string& delimeter = " ") const
    {
        vector<string> result;
        //boost::split(result, s, 
        //    boost::is_any_of(delimeter),
        //    boost::token_compress_on
        //);

        return result;
    }

};


int main()
{
    // Voltage (5v, 220v)
    // Socket/plug type (Europe, UK, USA)
    // Support of 120/220V

    // Adapt interface X to Y (aka wrapper that slice/or/extends interface)
    std::cout << "Hello World! Adapter \n";

    std::stack<int> s;
    s.push(123);
    int x = s.top();
    s.pop();


    //boost::to_lower(s);
    vector<string> parts;
    //boost::split(parts, s, boost::is_any_of(" "));
    for (const auto& p : parts)
    {
        cout << "[" << p << "]" << endl;
    }

    // does not violate open-closed principle
    // but intellsense will not show it

    StringAdapter sa { "Hello world" };
    vector<string> parts2 = sa.to_lower().split();

    for (const auto& p : parts2)
    {
        cout << "[[" << p << "]]" << endl;
    }

    char a;
    cin >> a;
    return 0;
}

/*
    side Notes 

    STD
    template <class _Mydeque>
    class _Deque_unchecked_const_iterator

    STD
    template <class _Ty, class _Container = deque<_Ty>>
    class stack

    stack #pragma pack
    https://stackoverflow.com/questions/3318410/pragma-pack-effect
    pack pragma
    https://docs.microsoft.com/en-us/cpp/preprocessor/pack?view=msvc-170
    /Zp (Struct Member Alignment) 
    https://docs.microsoft.com/en-us/cpp/build/reference/zp-struct-member-alignment?view=msvc-170

    Allocator
    Memory resource, etc.


    incl. boost
    https://stackoverflow.com/questions/45974108/how-to-add-boost-library-1-65-or-1-64-to-visual-studio-2017-project
    https://www.boost.org/doc/libs/1_65_0/more/getting_started/windows.html


    #TODO
*/