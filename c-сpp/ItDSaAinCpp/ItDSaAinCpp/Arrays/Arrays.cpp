﻿// Arrays.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "IntArray.h"
#include "IndexOutOfBoundsException.h"
using namespace std;

ostream & operator<<(ostream & os, const IntArray &a)
{
	os << "[";
	for (auto i = 0; i < a.Size(); i++) {
		os << a[i] << ", ";
	}
	os << "]";
	return os;
}

void swap(IntArray& a, IntArray& b) noexcept
{
	//Member wise swap
	swap(a.m_ptr, b.m_ptr);
	swap(a.m_size, b.m_size);

}

int main()
{
    
	// clang++ -std=c++14 -fsanitizer=address -g -o1 Array.cpp
	// -g : debug info
	// -o1 : optimization

	cout << "Creating empty array" << endl;
	IntArray a = {};
	cout << "a.Size() is " << a.Size() << endl;
	// delete a;

	cout << "----------------------------------" << endl;


	cout << "Creating an array containing 10 elements" << endl;
	IntArray b{ 10 };
	cout << "b.Size() is " << b.Size() << endl;
	// delete b;


	try
	{
		auto testException = b[11];
	}
	catch (const IndexOutOfBoundsException e)
	{
		cout << "Out of boundary";
	}

	// ---------------------------------------

	IntArray c{ 10 };
	for (auto i = 0; i < c.Size(); i++) 
	{
		c[i] = (i + 1) * 10;
	}


	cout << "Array `c`" << endl;
	cout << c << endl;

	// -----------------------------------

	IntArray f = c; // call default copy constructor which simply copy properties


	cout << "Array `f (swap copy)`" << endl;
	cout << f << endl;

	// --------------------

	IntArray d = std::move(f);
	cout << "Array `f (move)`" << endl;
	cout << d << endl;
}