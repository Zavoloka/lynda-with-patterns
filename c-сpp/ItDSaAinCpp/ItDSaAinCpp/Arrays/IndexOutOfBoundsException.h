#pragma once
#include <exception>


class IndexOutOfBoundsException : std::exception
{
public:
	IndexOutOfBoundsException();
	~IndexOutOfBoundsException();
};

