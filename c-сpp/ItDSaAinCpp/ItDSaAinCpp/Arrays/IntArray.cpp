//#include "pch.h"
#include "IntArray.h"
#include "IndexOutOfBoundsException.h"



bool IntArray::_isValidIndex(int index) const {

	return ((index >= 0) && (index < m_size));
}


IntArray::IntArray(int size)
{
	if (size != 0) 
	{
		m_ptr = new int[size] {};
		m_size = size;
	}
}


IntArray::IntArray(const IntArray& source) 
{

	// Allocate memory
	// Perform deep copy

	cout << "Constructor" << endl;

	if (source.IsEmpty())
	{
		m_size = 1;
		m_ptr = new int[m_size] {};
		return;
	}


	m_size = source.m_size;
	m_ptr = new int[m_size]{};

	for (int i = 0; i < m_size; i++)
	{
		m_ptr[i] = source.m_ptr[i];
	}


}

/*
IntArray& IntArray::operator=(const IntArray& source)
{
	cout << "Const copy assigment is called" << endl;

	// prevent self-copy
	if ((&source == this) || source.IsEmpty())
	{
		return *this;
	}

	// deep copy
	m_size = source.m_size;
	m_ptr = new int[m_size] {};

	for (int i = 0; i < m_size; i++)
	{
		m_ptr[i] = source.m_ptr[i];
	}

	// release prev. array block(?)
	
	return *this;
} */

IntArray& IntArray::operator=(IntArray source)
{
	cout << "Non-Const copy assigment is called" << endl;

	swap(*this, source);
	return *this;
}

IntArray::IntArray(IntArray && source)
{

	cout << "Move constructor was called" << endl;

	m_ptr = source.m_ptr;
	m_size = source.m_size;

	source.m_ptr = nullptr;
	source.m_size = 0;

}

int& IntArray::operator[](int index) {

	if (!_isValidIndex(index))
	{
		throw IndexOutOfBoundsException();
	}


	return m_ptr[index];
}

int IntArray::operator[](int index) const {

	if (!_isValidIndex(index))
	{
		throw IndexOutOfBoundsException();
	}

	return m_ptr[index];
}


int IntArray::Size() const
{
	return m_size;
}

bool IntArray::IsEmpty() const
{
	return (m_size == 0);
}

IntArray::~IntArray()
{
	delete[] m_ptr;
}
