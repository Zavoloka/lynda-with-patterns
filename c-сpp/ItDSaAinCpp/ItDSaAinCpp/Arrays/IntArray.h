#pragma once
#include <iostream>
using namespace std;

class IntArray
{

private:
	
	int* m_ptr{ nullptr };
	int m_size{ 0 };

	bool _isValidIndex(int index) const;

public:

	IntArray() = default;

	// copy constructor
	IntArray(const IntArray& source);
	// copy assignment
	//IntArray& operator=(const IntArray& source);

	// copy-and-swap (+/- becouse of arg copy)
	IntArray& operator=(IntArray source);

	IntArray(IntArray&& source);

	// https://habr.com/ru/post/436296/  explicit
	explicit IntArray(int size); //forbids constructors such IntArray = 3;

	int& operator[](int index);
	int operator[](int index) const;

	int Size() const;
	bool IsEmpty() const;

	virtual ~IntArray();


	friend void swap(IntArray& a, IntArray& b) noexcept;
};

