#pragma once

template <typename T>
class TemplateArray
{

private :

	T* m_ptr{ nullptr };
	int m_size{ 0 };


	bool _isValidIndex(int index) const
	{
		return ((index >= 0) && (index < m_size));
	}



public:

	// Default constructor
	TemplateArray() = default;


	// Constructor which considering size
	explicit TemplateArray(int size)
	{
		//assert(size >= 0);
		if (size != 0)
		{
			m_ptr = new T[size]{};
			m_size = size;
		}
	}

	// Copy constructor
	TemplateArray(const TemplateArray& source)
	{
		if (!source.IsEmpty())
		{
			m_size = source.m_size;
			m_ptr = new T[m_size]{};

			for (int i = 0; i < m_size; i++)
			{
				m_ptr[i] = source.m_ptr[i];
			}
		}
	}


	~TemplateArray()
	{
		delete[] m_ptr;
	}
};

// -------------------------------------------------

