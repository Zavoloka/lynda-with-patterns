#pragma once

#include <iostream>


template <typename T>
class LinkedListImpl
{

private:


	// Node struct
	struct Node
	{

		// Store pointer for the next Node
		Node* Next{nullptr};

		// Store actual data
		T Data{};

		// ---------------------------------------------------------------

		// Create a default empty node
		Node() = default;

		// Create a node storing input data
		explicit Node(const T& data) : Data{ data } {};

		// Create a node  storing input data, and pointing to another node
		Node(const T& data, Node* next) : Data{ data }, Next{ next }{};

	}


	// Pointer to the head
	Node* m_head{ nullptr };

	// Amount of elements
	int m_count{ 0 };


	// Forbids Copy and Assignment constructors
	LinkedListImpl(const LinkedListImpl&) = delete;
	LinkedListImpl& operator=(const LinkedListImpl&) = delete;




public:

	// ??
	typedef Node* Position;


	LinkedListImpl() = default;

	// Returns amount of elements inside list
	int Count() const
	{
		return m_count;
	}

	// Check if the list is empty
	bool IsEmpty() const
	{
		return (m_count == 0);
	}

	void Clear()
	{
		while (!IsEmpty())
		{
			//RemoveHead();
		}

	}






	~LinkedListImpl() 
	{
		//Clear();
	};
};

