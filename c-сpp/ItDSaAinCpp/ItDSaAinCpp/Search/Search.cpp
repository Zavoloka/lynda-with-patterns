﻿// Search.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include "pch.h"
#include <iostream>
#include <vector>


using namespace std;


constexpr int kNotFound = -1;

int Search(int element, const int* v, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (v[i] == element)
		{
			return i;
		}
	}

	return kNotFound;

}

int BinarySearch(int element, const int* v, int size)
{

	int left = 0;
	int right = size - 1;

	while (left <= right)
	{

		int middle = (left + right) / 2;

		if (v[middle] == element)
		{
			return middle;
		}
		else if (v[middle] < element) 
		{
			// check edge cases
			left = middle + 1; 
		}
		else if (v[middle] > element)
		{
			right = middle - 1;
		}
	}

	return kNotFound;
}


void Print(const vector<int> & v)
{
	cout << "[ ";
	for (int x : v)
	{
		cout << x << ' ';
	}
	cout << "]\n";

}

int main()
{
    std::cout << "Hello World!\n"; 

	vector<int> v{ 33, 44, 55, 66, 11, 22 };
	vector<int> v2{ 11, 22, 33, 44, 55, 66 };
	Print(v);
	Print(v2);

	cout << "Element to search?\n";
	int x;
	cin >> x;

	auto const pos = Search(x, v.data(), v.size());
	auto const pos2 = BinarySearch(x, v2.data(), v2.size());
	if (pos == kNotFound)
	{
		cout << "Element not found. \n";
	}else{
		cout << "Element was found at index " << pos << " \n";
	}

	cout << "Pos2 " << pos2 << "\n";

	cin >> x;
}
