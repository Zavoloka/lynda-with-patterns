﻿// Stack.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

// https://devblogs.microsoft.com/cppblog/precompiled-header-pch-issues-and-recommendations/
//#include "pch.h"
#include <iostream>
#include "StackImpl.h"


int main()
{
    std::cout << "Hello World!\n"; 

	StackImpl<int> aStack = StackImpl<int>{ 2 };
	aStack.Push(3);
	aStack.Push(6);

	cout << "Stack print" << endl;
	cout << aStack << endl;

	cout << "Stack top" << endl;
	cout << aStack.Top() << endl;

	auto p = aStack.Pop();
	cout << "Stak pop : " << p << endl;

	cout << "Stack print" << endl;
	cout << aStack << endl;


}

