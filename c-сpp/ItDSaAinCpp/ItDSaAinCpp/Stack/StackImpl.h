#pragma once

#include <ostream>
#include <vector>

using namespace std;

/*
		TODO:
	--------------------------
	+Push, Pop, Top
	Size, IsEmpty
	Print stream << overloads

	demo example
	assert check
	exception on overflow

*/

template <typename T>
class StackImpl
{
private:

	vector<T> m_vector;
	int m_top;
	int size;

public:
	
	// constructor
	explicit StackImpl(int maxStackSize) :  size{ maxStackSize }, m_top{ -1 }
	{
		m_vector.reserve(maxStackSize);
	};


	bool IsEmpty()
	{
		return (m_top == -1);
	};


	int Size()
	{
		// to avoid calling m_array.size() each time
		return size;
	}


	bool Push(const T& val) 
	{
		m_top++;
		if (m_top >= size) 
		{
			m_top--;
			return false;
		}
		
		m_vector.push_back(val); // how do we handle a complex objects with assigment operator;
		return true;

	};

	T Pop()
	{
		T topElement = m_vector[m_top];
		m_vector[m_top] = 0;
		m_top--;
		return topElement;
	};

	const T& Top()
	{
		return m_vector[m_top];
	};


	friend std::ostream& operator <<(std::ostream& os, StackImpl<T>& stack)
	{
		if (stack.IsEmpty())
		{
			os << "** Stack is empty\n";
			return os;
		}

		os << "** Stack : \n";
		for (int i = stack.m_top; i > -1; i--)
		{
			auto var = stack.m_vector[i];
			os << " Stack element : " << var << "\n";
		}


		return os;

	}


	~StackImpl() 
	{
		m_top = -1;
		// destroy array
	};
};

