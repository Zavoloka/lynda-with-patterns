// CPP11Features.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <typeinfo>
#include <vector>


using namespace std;


auto simpleTest() -> int {

	return 2;
}


template <class T, class S>
auto templateTest(T value1, S value2) -> decltype(value1 + value2) {

	return value1 + value2;
}

int get() {
	return 7;
}

auto get2() -> decltype(get()) {
	return get();
}



int main()
{
	
	int i;
	string s;

	cout << "Type ID demo : " << endl;
	cout << "Int : " <<  typeid(i).name() << endl;
	cout << "String : " << typeid(s).name() << endl;
	
	decltype(s) name = "Bob";

	// --------------------------------------------------------------

	cout << endl;
	cout << "decltype + auto exmaple " << endl;
	cout << "template : " << templateTest<int, float>(2, 3) << endl;
	cout << "function in function : " << get() << endl;

	// --------------------------------------------------------------

	int a;
	cin >> a;


	// ------------------- range based loops ------------------------

	cout << "Range based loop example : " << endl;


	cout << "Strings //" << endl;
	auto texts = { "one", "two", "three" };
	for (auto text : texts) {
		cout << text  << " " << endl;
	}


	cout << "Numbers //" << endl;
	std::vector<int> numbers; 
	numbers.push_back(2);
	numbers.push_back(3);
	numbers.push_back(4);


	for (auto number : numbers) {
		cout << number << " " << endl;
	}

	cout << "String and chars //" << endl;
	string hello = "Hello";
	for (auto c : hello) {

		cout << c << endl;
	}





	return 0;
}

