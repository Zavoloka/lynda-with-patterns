﻿// ConstructorsLRValuesCasts.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>


/*

class Parent()
{

	//  #DelegatingConstructors

	Parent(): Parent("hello:){};
	Parent(string text){};
}

*/

int main()
{


	//  #ElisionAndOptimization
	// 57
	/*

	Test getTest()
	{
		return Test();
	}
	
	Test test1 = getTest();
	cout << test1 << endl;

	// =>> constructor, copy constructor, destructor, copy constructor, destructor ...  // when optimization is off

	*/





    std::cout << "Hello World!\n"; 
}

