// FuncPointersAndNewFeatures.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>


#include <algorithm>
#include <vector>


using namespace std;


// function pointer test
void test(int value) {

	cout << "Hello" << value << endl;
}



// pass function to other function test
bool match(string testString) {

	return testString.size() == 3;

}

int matchCount(vector<string> &texts, bool(*match)(string test)) {

	int total = 0;
	for (auto textString : texts)  total += match(textString);
	return total;
}



int main()
{


	cout << "Pointer function" << endl;

	test(5);

	// declare function pointer
	void(*pTest)(int);
	
	//pTest = &test;   --- '&' is not mandatory, because of the name of the function is a Pointer to the function

	// call a function by pointer
	//(*pTest)(6);      --- '*' is not mandatory

	// -------------------------------------

	pTest = test;
	pTest(6);


	// -----------------------------------------

	cout << "Passing function to another function" << endl;


	// dummy data
	vector<string> texts;

	texts.push_back("one");
	texts.push_back("two");
	texts.push_back("three");
	texts.push_back("four");

	texts.push_back("one");
	texts.push_back("two");
	texts.push_back("three");
	texts.push_back("four");


	cout << "Match 'one' : " << match("one") << endl;

	cout << "Match count_if : " << count_if(texts.begin(), texts.end(), match) << endl;

	cout << "Count matches : " << matchCount(texts, match) << endl;
	
	cout << "Count matches (lambda) : " << matchCount(texts,  [](string a) -> bool { return a.size() == 3; }) << endl;



	// -------------------------------------

	int delay = 0;
	cin >> delay;
    return 0;
}

