// FunctorsDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

// parent virtual struct for functor
struct Test {

	virtual bool operator()(string &text, string &text2) const = 0;
	virtual ~Test(){};
};

struct MatchTest : public Test {

	virtual bool operator()(string &text, string &text2) const override {
		return text == text2;
	};

};


// check function which incorporate functor
void check(string text, Test &testObj) {

	string lionString = "lion";
	if ( testObj(text, lionString) ) {
		cout << "Does match" << endl;
		return;
	}

	cout << "Dosn't match." << endl;

}




int main()
{

	cout << "Functors demo :" << endl;

	string trueLion = "lion";
	MatchTest mt;
	
	check(trueLion, mt);


	// ---------------------
	int a;
	cin >> a;
			
    return 0;
}

