﻿// InitializationAndLambdas.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include <initializer_list>
#include <algorithm>
#include <functional>

using namespace std;


class Test {
public : 

	// https://en.cppreference.com/w/cpp/utility/initializer_list
	Test(initializer_list<string> texts) {
		for (auto value : texts) {
			cout << value << endl;
		}

	}

	void print(initializer_list<string> texts) {
		for (auto value : texts) {
			cout << value << endl;
		}
	}

};



class TestObjInit {
	int id{ 3 };
	string name{ "Mike" };

public:

	TestObjInit() = default;
	TestObjInit(int id) :id(id) {};

	void print() {
		cout << id << ": " << name << endl;
	};

};



void lampdaExecutor(void(*pFunc)())
{
	pFunc();
}

// takes pointer on function with special signature for string parameter
void lambdaExecutorAgain(void (*greet)(string))
{
	greet("Bob");
}


// takes pointer on function with special signature for string parameter
void lambdaExecutorAgainWithParameters(double (*divide)(double a, double b ))
{
	auto rval = divide(9, 3);
	cout << "Divide3 lambda : " << rval << endl;
}

// --------------------------------------------------

bool check(string &test)
{
	return test.size() == 3;
}

class Check
{
public:
	bool operator()(string &test)
	{
		return test.size() == 5;
	}
} check1Class;


void runCheck(function<bool(string&)> check)
{
	string test = "dog";
	cout << check(test) << endl;
}

int main()
{
    std::cout << "Hello World!\n Initialization and Lambdas"; 


	// C++98
	// -----------------------
	int values[] = { 1,4,5 };
	class C{

	public :
		string text;
		int id;

	};

	C c1 = { "Hello", 7 };

	struct {

		string text;
		int id;

	} r1 = { "Hello", 7 }, r2 = { "Hi", 9 };


	// C++11
	//------------------------
	int numbers[]{ 1, 2, 3 };

	int *pInts = new int[3]{ 1, 2, 3 };  // works only when you specify count of elements
	delete pInts;


	// both are equal to nullptr
	int *pSmmth{ nullptr };
	int *pSmthAgain{};


	struct {
		int value;  // int value = 7 will break default init from constructor
		string text;
	} s1{ 5, "Hi" };


	vector<string> strings{ "one", "two", "three" };

	// C++11

	Test test{ "apple", "orange", "banana" };
	test.print({ "one", "two", "three", "four" });


	//TestObjInit testObj;
	//testObj.print();


	// Lambdas
	// -----------------------------

	auto func = []() { cout << "Hello from lambda" << endl; };
	func();
	lampdaExecutor(func);
	lampdaExecutor([]() { cout << "Hello from lambda again" << endl; });

	auto pGreet = [](string name) { cout << "Hello " << name << endl; };
	pGreet("Mike");
	lambdaExecutorAgain(pGreet);

	auto pDivide = [](double a, double b) 
	{
		return a / b;
	};

	cout << "Divide lambda : " << pDivide(10.0, 5.0) << endl;

	// specify the return type
	auto pDivide2 = [](double a, double b) -> double
					{
						if (b == 0.0) 
						{
							return 0;
						}
						return a / b;
					};


	cout << "Divide2 lambda : " << pDivide2(10.0, 0) << endl;

	lambdaExecutorAgainWithParameters(pDivide2);
	

	int one = 1;
	int two = 2;
	int three = 3;

	// Capture by value 
	[one, two]() {cout << one << ", " << two << endl; }();


	// Capture all local variables by value 
	[=]() {cout << one << ", " << two << endl; }();


	// Default capture all variables by value, but "three" by reference
	[=, &three]() { three = 7; cout << one << ", " << two << endl; }();
	cout << three << endl;


	// Default capture all variables by reference
	[&]() {three = 7, two = 8, cout << one << ", " << two << endl; }();
	cout << two << endl;

	
	// Default capture all by ref, but two by value
	[&, two, three]() {one = 100; cout << one << ", " << two << endl; }();
	cout << one << endl;

	// we also can capture (this) in lambdas	[this](){}		YES
	// we can do								[&, this](){}	YES
	// we can't do								[=, this](){}	NO

	// ------------------------------------------------------------------------------------
	//  #TheStandartFunctionType
	vector<string> vec{ "one", "two", "three"};
	int size = 5;
	auto lambda = [size](string test) { return test.size() == size; };

    int count = count_if(vec.begin(), vec.end(), lambda);
	cout << "#TheStandartFunctionType : 1st count " << count << endl;

	count = count_if(vec.begin(), vec.end(), check);
	cout << "#TheStandartFunctionType : 2nd count " << count << endl;


	count = count_if(vec.begin(), vec.end(), check1Class);
	cout << "#TheStandartFunctionType : 3nd count " << count << endl;

	runCheck(lambda); // also works to pointers to the functions 

	function<int(int, int)> add = [](int one, int two) {return one + two; };
	cout << add(7, 3) << endl;

	// ------------------------------------------------------------------------------------
	// #MutableLambdas

	int cats = 5;
	[cats]() mutable // behaves like a variable which was passed to a function
	{
		cats = 8;
		cout << "#MutableLambdas cats-1 : " << cats << endl;
	}();

	cout << "#MutableLambdas cats-2 : " << cats << endl;


	// --------------------------------------------------------

	int a;
	cin >> a;

	return 0;


}



