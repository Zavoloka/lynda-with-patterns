﻿// NestedTemplaytClasses.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "Ring.h"

#include <string>


int main()
{

	// How to enable c+++17 in VS17
	// https://docs.microsoft.com/en-Us/cpp/build/reference/std-specify-language-standard-version?view=vs-2019


	Ring<int> rObjInt(3);
	rObjInt.Add(1);
	rObjInt.Add(2);
	rObjInt.Add(3);
	rObjInt.Add(4);
	rObjInt.Add(5);


	cout << "Ring <int> test" << endl;
	cout << rObjInt.GetElement(0) << " | " << rObjInt.GetElement(1) << " | " << rObjInt.GetElement(2) << endl;


	// C++98
	cout << "Ring iteration (98) : " << endl;
	for (Ring<int>::iterator it = rObjInt.begin(); it != rObjInt.end(); it++) {

		cout << *it << ", ";
	}
	cout << endl;


	// C++11
	cout << "Ring iteration (11) : " << endl;
	for (auto value:rObjInt) {

		cout << value << ", ";
	}
	cout << endl;


	// string test

	Ring<string> rObjStr(2);
	rObjStr.Add("hey");
	rObjStr.Add("ha");
	rObjStr.Add("hoh");

	cout << "Ring <string> test" << endl;
	cout << rObjStr.GetElement(0) << " | " << rObjStr.GetElement(1) << endl;




	// --------------------------

	std::cout << "Hello World!\n";
	int n;
	std::cin >> n;


	



}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
