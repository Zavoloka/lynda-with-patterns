#pragma once
#ifndef RING_H
#define RING_H

#include <iostream>
using namespace std;

// --------- declaration ---------

// template <typename T> also ok
template < class T >
class Ring
{
private:
	unsigned int _size = 0;
	T* _storage = nullptr;

public:

	class iterator;

protected:

	unsigned int _defaulSize = 2;
	unsigned int _index = 0;

	
public:


	// delegating constructor C++11
	// https://stackoverflow.com/questions/308276/can-i-call-a-constructor-from-another-constructor-do-constructor-chaining-in-c 

	inline Ring(): Ring(_defaulSize){}
	inline Ring(unsigned int n) : _size(n) { /* exeption check, n check*/ _storage = new T[_size]; }

	Ring(const Ring &) = delete;  // forbids copy constructor
	Ring(Ring &&) = delete; // forbids move constructor

	inline ~Ring() { delete [] _storage; }



	// rewrite to smart pointers
	// exception handler
	// boost signal https://stackoverflow.com/questions/238738/events-in-c

	void Add(T val);
	inline unsigned int GetSize() const { return _size; };
	inline unsigned int GetIndex() const { return _index; };
	T GetElement(unsigned int index);

	T operator++(int); // postfix increment operator
	iterator begin() {

		return Ring<T>::iterator(0, *this);
	};
	iterator end() {

		return iterator(_size, *this);
	};

private:

	void _increaseIndex();


};


// --------- implementation ---------

template < class T >
void Ring<T>::_increaseIndex(){

	_index++;
	_index = (_index < _size) ? _index : 0;

};


template < class T >
void Ring<T>::Add(T val) {

	// TODO: probably check if array was initialized properly
	_storage[_index] = val;
	_increaseIndex();

};



template < class T >
T Ring<T>::GetElement(unsigned int index) {

	// TODO: better to throw an exception here
	if (index >= _size) {
		index = _size - 1;
	}

	return _storage[index];
};

template < class T >
T Ring<T>::operator++(int) {

	// https://docs.microsoft.com/en-us/cpp/cpp/increment-and-decrement-operator-overloading-cpp?view=vs-2019

	Ring<T> temp = *this; 
	temp._increaseIndex;
	return temp;

};


// TODO : Recheck this implementation
// --------- iterator implementation ---------//
template<class T>
class Ring<T>::iterator{

private:

	unsigned int _index;
	Ring &_iRing;

public: 

	inline iterator(unsigned int index, Ring &iRing) : _index(index), _iRing(iRing) {};

	iterator &operator++(int) {

		_index++;
		return *this;
	};

	iterator &operator++() {
		_index++;
		return *(this);
	};

	T operator*() {
		return _iRing.GetElement(_index);
	};

	bool operator==(const iterator &other) const {

		return other.GetIndex() == GetIndex();
	};

	bool operator!=(const iterator &other) const {
		
		return !(other == (*this));
	};

	unsigned int GetIndex() const { return _index;  };


	void print() {
		cout << "Hello from iterator" << endl;
	};

};
#endif /*RING_H*/