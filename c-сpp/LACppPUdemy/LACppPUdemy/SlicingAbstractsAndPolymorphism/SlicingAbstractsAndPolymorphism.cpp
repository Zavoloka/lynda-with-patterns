﻿// SlicingAbstractsAndPolymorphism.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

using namespace std;


// --------------------------------

class Parent {

private:

	int one;

public:

	

	Parent() : one(0) {}

	Parent(const Parent &other) : one(0) {


		one = other.one;
		cout << "copy parent" << endl;
	}


	void print() {
		cout << "parent" << endl;
	}


	virtual ~Parent() {};
};


// class inheritance is private by default
// When you privately inherit from a class or a struct, you explicitly say, 
// among others, that direct conversion from a derived type to a base type isn't possible.
// https://stackoverflow.com/questions/10472848/conversion-from-derived-to-base-exists-but-is-inaccessible

class Child : public Parent {

private:

	int two;

public:

	Child() : two(0) {

	}

	void print() {

		cout << "child" << endl;
	}
};


// ---------------------------------------------------
// Abstract classes and pure Virtual Functions examples 

// Abastract class with abstract functions
class Animal {

public :

	// abstract functions
	virtual void run() = 0;
	virtual void speak() = 0;
};

// Still abstract class, because it doesn't implement run()
class Dog : public Animal {

public: 
	virtual void speak() {
		cout << "Woof!" << endl;
	}

};


class Labrador : public Dog {

public :
	Labrador() {

		cout << "new labrador" << endl;
	}

	virtual void run() {

		cout << "Labrador running" << endl;
	}


};


void test(Animal &a) {

	a.run();
}



int main()
{

	cout << "Slicing exmaple" << endl;

	Child c1;
	Parent &p1 = c1;
	p1.print();


	// copy initialization
	Parent p2 = Child();
	p2.print();


	// -----------------------
	cout << endl;
 
	cout << "Abstract classes example" << endl;

	Labrador lab;
	lab.run();
	lab.speak();


	Labrador labs[5];
	Animal *animals[5]; // still, we can declare a pointer for abstract class
	animals[0] = &lab;
	animals[0]->speak();



	// ----------------
	int a = 0;
	cin >> a;

	return 0;

}

