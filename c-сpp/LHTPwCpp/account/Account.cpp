#include <sstream>
#include "Account.h"

 Account::Account():balance(0), limit(100)
 // what 100, $, pounds or YAH
 {


 }; // default constructor

// more like 'show amount'
std::vector<std::string> Account::Report(){

    std::vector<std::string> logVector;
    std::stringstream ss;
    
    ss << "Balance : " << balance;

    logVector.push_back(ss.str());

    ss.str("");
    ss << "Account : " << limit;
    logVector.push_back(ss.str());

    return logVector;

};

bool Account::Deposit(int amount){

    // move to seprate validation method
    if (amount <= 0){
        return false;
    }

    /*  LOG, implement DI (boost)
        + decorator for banking fee
    	
        log.push_back(Transaction(amt,"Deposit"));
		balance -= 1;
		log.push_back(Transaction(1,"Service Charge"));
    */

    balance += amount;
    return true;

};
bool Account::Withdraw(int amount){

    if (amount <= 0){
        return false;
    }

    if (balance + limit < amount){
        return false; // not enough funds
    }

    /*
	balance -= amt;
			log.push_back(Transaction(amt,"Withdraw"));
			balance -= 1;
			log.push_back(Transaction(1,"Service Charge"));
			return true;
    */

    balance -= amount;

    return true;
};

