#include <vector>
#include <string>

#include "Transaction.h"

class Account
{
    private:
        int balance;
        std::vector<Transaction> log;
        int limit;

    public:
        Account(); // default constructor
        std::vector<std::string> Report();

        // TODO : add overloads for transactions
        bool Deposit(int amount);
        bool Withdraw(int amount);

        //  inline function 
        int GetBalance(){return balance;} 


};