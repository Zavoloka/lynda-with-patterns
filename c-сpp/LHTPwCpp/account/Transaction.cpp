#include "Transaction.h"
#include <sstream>

using namespace std;



Transaction::Transaction(){

   // hello, dummy constructor is here 
   
};
 
Transaction::Transaction(int amount, Transaction::transactionType transaction, 
                    std::pair<int, std::string> sender, 
                    std::pair<int, std::string> receiver):

                    amount(amount),  tType(transaction),
                    bankSender(sender), bankReceiver(receiver)

{

   // hello, dummy constructor is here 
};

std::string Transaction::Report(){

    std::stringstream response;
    response << "Transaction #N " << endl
             << "BankSender : " << std::to_string(bankSender.first)
             << "/" << bankSender.second << endl;

    response << "BankReceiver : " << std::to_string(bankReceiver.first)
             << "/" << bankReceiver.second << endl;

    response << "Amount : " << amount << endl;
    response << "Transaction type : " << tType;
    // TODO : extract enum value name later


    return response.str();
};