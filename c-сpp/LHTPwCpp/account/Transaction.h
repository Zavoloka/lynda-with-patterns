#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <string>
#include <chrono>


class Transaction
{
    private:
        int amount;
 

        // time of transaction
        //std::chrono::time_point time;

        // first element : mank MFO (a shortcut code to identify bank in Ukraine)
        // second element : bank name
        // https://uk.wikipedia.org/wiki/%D0%9C%D0%A4%D0%9E_%D0%BA%D0%BE%D0%B4_%D0%B1%D0%B0%D0%BD%D0%BA%D1%83
        std::pair<int, std::string> bankSender;
        std::pair<int, std::string> bankReceiver;
        
        // TODO: we can use instance of the bank class here, later
        // TODO: add user/client names with phones 
        // (use it inside inherited version of transaction)


    public:
        
        enum transactionType {PURCHASING, TRANSFERRING, PAYBACK} tType;

        // a dummy constructor
        Transaction();
 
        Transaction(int amount, transactionType transaction, 
                    std::pair<int, std::string> bankSender, 
                    std::pair<int, std::string> bankReceiver);
        
        std::string Report();
        // copy constructor, move construct, ext, ext
        // sorting, search, operator overloading;
};

#endif