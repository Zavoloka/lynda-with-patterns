
#include <iostream>
#include "Transaction.h"
#include "Account.h"


int main(int argc, char **argv)
{
  
  Transaction * tr1 = new Transaction(
                              50, Transaction::PURCHASING, 
                              std::make_pair<int, std::string>(233, "PrivatBank"),
                              std::make_pair<int, std::string>(422, "UkrsibBank")
                            ); 
  std::string tr1Report = tr1->Report();

  std::cout << "Hello banking world!" << std::endl; 
  std::cout << tr1Report << std::endl;


  std::cout << "\n Account test \n" << std::endl; 
  Account * ac1 = new Account();
  ac1->Withdraw(25);
  std::vector<std::string> acLog = ac1->Report();

  for (auto element:acLog){
    std::cout << "los: " << element << std::endl;
  }

  return 0;
}