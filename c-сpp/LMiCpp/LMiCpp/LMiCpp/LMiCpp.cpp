﻿
#include <iostream>
#include <thread>
#include <chrono>

using namespace std;

void work()
{
    for (int i = 0; i < 10; i++)
    {
        this_thread::sleep_for(chrono::milliseconds(500)); 
        cout << "Loop " << i << endl;
    }
}

int main()
{
    thread t1(work);
    thread t2(work);

    t1.join();
    t2.join();

    cout << "Hello World!\n";

    //https://github.com/caveofprogramming/Cpp-Multithreading
}


// asynchronous execution
// time slicing
// user input
// server response