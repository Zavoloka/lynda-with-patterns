﻿// LMiCppMutex.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <mutex>

using namespace std;

void work(int& count, mutex& mtx)
{
    const int iterations = static_cast<int>(1E4);
    for (int i = 0; i < iterations; i++)
    {
        mtx.lock();
        ++count;
        mtx.unlock();
    }
}

void work2RII(int& count, mutex& mtx)
{
    //https://en.cppreference.com/w/cpp/language/raii
    const int iterations = static_cast<int>(1E4);
    for (int i = 0; i < iterations; i++)
    {
        lock_guard<mutex> guard(mtx);
        ++count;
    }
}

int main()
{
    // ------------------------------------------------
    // atomics

    std::cout << "Hello World!\n";

    atomic<int> count = 0;
    const int ITERATIONS = static_cast<int>(1E3);

    thread t1([&count, ITERATIONS]()
        {
            for (int i = 0; i < ITERATIONS; i++)
            {
                ++count;
            }
        }
    );

    thread t2([&count, ITERATIONS]()
        {
            for (int i = 0; i < ITERATIONS; i++)
            {
                ++count;
            }
        }
    );


    t1.join();
    t2.join();

    cout << "Final t1,t2 count: " << endl;
    cout << count << endl;


    // ------------------------------------------------
    // mutex lock/unlock

    int count2 = 0;
    mutex mtx;

    auto func = ([&count2, &mtx, ITERATIONS]()
        {
            for (int i = 0; i < ITERATIONS; i++)
            {
                mtx.lock();
                ++count2;
                mtx.unlock();
            }
        }
    );


    thread t3(func);
    thread t4(func);

    t3.join();
    t4.join();

    cout << "Final t3,t4 count: " << endl;
    cout << count << endl;


    //-----------------------------------------------
    // mtx by ref

    int count3 = 0;
    mutex mtx2;

    thread t5(work, ref(count3), ref(mtx2));
    thread t6(work, ref(count3), ref(mtx2));

    t5.join();
    t6.join();

    cout << "Final t5,t6 count: " << endl;
    cout << count3 << endl;


    //-----------------------------------------------
    // mtx RII style

    int count4 = 0;
    mutex mtx3;

    thread t7(work2RII, ref(count4), ref(mtx3));
    thread t8(work2RII, ref(count4), ref(mtx3));

    t7.join();
    t8.join();

    cout << "Final t7,t8 count: " << endl;
    cout << count4 << endl;



    return 0;
}