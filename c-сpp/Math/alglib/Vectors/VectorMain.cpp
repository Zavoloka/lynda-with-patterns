#include <iostream>

// include alglib module to work with vectors/matrixes
#include "ap.h"

using namespace std;

int main(){

    alglib::real_1d_array a("[1,99,3,4,5]");
    double b[] = {5, 6};

    // automatically get size of b
    a.attach_to_ptr(2, b);

    cout << "2nd element : " << a[1] << endl;
    cout << "7th element : " << a(6) << endl;

    return 0;
}