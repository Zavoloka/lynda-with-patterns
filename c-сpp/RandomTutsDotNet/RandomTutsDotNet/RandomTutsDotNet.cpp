﻿// RandomTutsDotNet.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;

#include <boost/signals2.hpp>
using namespace boost::signals2;



template <typename T>
class INotifyPropertyChanged
{
public:
	signal<void(const T*, string)> PropertyChanged;
};


// https://docs.microsoft.com/en-us/cpp/cpp/declspec?view=vs-2019
class Person : public INotifyPropertyChanged<Person>
{
public:
	int age;
	int get_age() const 
	{ 
		return age; 
	}
	void put_age(int value)
	{
		if (value == age)
		{
			return;
		}

		age = value;
		PropertyChanged(this, "Age");
	}

	__declspec(property(get = get_age, put = put_age)) int Age;

	// ----------------------------------------------------------------

	// can't be private
	bool get_can_vote()
	{
		return age >= 16;
	}

	// in C++ we don't have properties, so we use __declspec
	__declspec(property(get = get_can_vote)) bool CanVote;


};

int main()
{

	Person p;
	p.age = 22;
	cout << p.CanVote << endl;
	
	p.PropertyChanged.connect([](const Person *p, string name)
	{
		cout << "Person had their " << name << " changed to " << p->Age <<  endl;
	});

	p.Age = 33;
	p.Age--;

	// ---------------------------------------------------------------------------
	
	int a;
	cin >> a;

	return 0;
}
