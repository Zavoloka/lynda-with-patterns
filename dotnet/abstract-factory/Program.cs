﻿using System;
using AbstractFactory.Src;
using AbstractFactory.Src.Interfaces;
using AbstractFactory.Src.ShapeClasses;
using AbstractFactory.Src.ColorClasses;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Partly inspired by:
                    https://www.tutorialspoint.com/design_pattern/abstract_factory_pattern.htm
                
                TODO/TOREAD :  https://www.codeproject.com/Articles/21043/Generic-Abstract-Factory
            */


            AbstractFactoryClass<AbstractShape> shapeFactory = new ShapeFactory();
            
            AbstractShape circleShape   = shapeFactory.GetInstance("circle");
            circleShape.Draw();

            AbstractShape squareShape   = shapeFactory.GetInstance("square");
            squareShape.Draw();

            AbstractShape triangleShape = shapeFactory.GetInstance("triangle");
            triangleShape.Draw();

            // -------------------------------------------------------------------

            AbstractFactoryClass<AbstractColor> colorFactory = new ColorFactory();

            AbstractColor redColor   = colorFactory.GetInstance("red");
            redColor.Fill();

            AbstractColor greenColor = colorFactory.GetInstance("green");
            greenColor.Fill();

            AbstractColor blueColor  = colorFactory.GetInstance("blue");
            blueColor.Fill();
        }
    }
}
