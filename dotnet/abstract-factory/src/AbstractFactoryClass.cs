using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src
{
    public abstract class AbstractFactoryClass<T> : IFactory<T>
    {

        /* some fields/properties go here */

        // get instance of derived class
        public abstract T GetInstance(string type);
    } 
    
}