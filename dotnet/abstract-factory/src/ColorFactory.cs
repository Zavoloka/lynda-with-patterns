using System;
using AbstractFactory.Src.Interfaces;
using AbstractFactory.Src.ColorClasses;

namespace AbstractFactory.Src
{
    public class ColorFactory : AbstractFactoryClass<AbstractColor>
    {

        public override AbstractColor GetInstance(string type){

            switch(type)
            {

                case "red"   : return new RedColor();
                               //break; // resolve warnings
                
                case "green" : return new GreenColor();
                               //break; // resolve warnings

                case "blue"  :
                default      : return new BlueColor();
                               //break; // resolve warnings
            }
            
        }

    }
}