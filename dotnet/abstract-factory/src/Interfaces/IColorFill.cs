using System;

namespace AbstractFactory.Src.Interfaces
{
    
    public interface IColorFill
    {
        void Fill();
    }

}