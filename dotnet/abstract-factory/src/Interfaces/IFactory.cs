using System;

namespace AbstractFactory.Src.Interfaces
{
    // type T points to the type
    public interface IFactory<T>
    {
        T GetInstance(string type);
    }

}