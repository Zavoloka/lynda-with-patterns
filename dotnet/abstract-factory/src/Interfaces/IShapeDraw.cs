using System;

namespace AbstractFactory.Src.Interfaces
{
    
    public interface IShapeDraw
    {
        void Draw();
    }

}