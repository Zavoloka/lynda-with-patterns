using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src.ShapeClasses
{
    
    public abstract class AbstractShape : IShapeDraw
    {
        private double _area{

            get {return _area;} 
            set {_area = value;}
        }

        public abstract void Draw();
     
    }

}