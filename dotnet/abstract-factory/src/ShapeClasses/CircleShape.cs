using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src.ShapeClasses
{
    
    public class CircleShape : AbstractShape
    {
        public override void Draw(){
            Console.WriteLine("draw circle");
        }
    }

}