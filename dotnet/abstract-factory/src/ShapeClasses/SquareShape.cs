using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src.ShapeClasses
{
    
    public class SquareShape : AbstractShape
    {
        public override void Draw(){
            Console.WriteLine("draw square");
        }
    }

}