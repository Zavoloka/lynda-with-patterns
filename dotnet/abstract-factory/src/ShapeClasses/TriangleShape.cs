using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src.ShapeClasses
{
    
    public class TriangleShape : AbstractShape
    {
        public override void Draw(){
            Console.WriteLine("draw triangle");
        }
    }

}