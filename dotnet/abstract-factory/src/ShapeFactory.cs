using System;
using AbstractFactory.Src.Interfaces;
using AbstractFactory.Src.ShapeClasses;

namespace AbstractFactory.Src
{
    public class ShapeFactory : AbstractFactoryClass<AbstractShape>
    {

        public override AbstractShape GetInstance(string type){

             switch(type)
            {

                case "circle"   : return new CircleShape();
                                  //break; // resolve warnings
                
                case "square"   : return new SquareShape();
                                  //break; // resolve warnings

                case "triangle" :
                default         : return new TriangleShape();
                                  //break; // resolve warnings
            }

        }

    }
}