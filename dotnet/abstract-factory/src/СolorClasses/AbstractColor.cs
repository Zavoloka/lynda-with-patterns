using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src.ColorClasses
{
    
    public abstract class AbstractColor : IColorFill
    {
        private bool _mixable{

            get {return _mixable;} 
            set {_mixable = value;}
        }

        public abstract void Fill();
     
    }

}