using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src.ColorClasses
{
    
    public class BlueColor : AbstractColor
    {
        public override void Fill(){
            Console.WriteLine("fill blue");
        }
    }

}