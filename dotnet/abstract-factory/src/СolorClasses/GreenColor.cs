using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src.ColorClasses
{
    
    public class GreenColor : AbstractColor
    {
        public override void Fill(){
            Console.WriteLine("fill green");
        }
    }

}