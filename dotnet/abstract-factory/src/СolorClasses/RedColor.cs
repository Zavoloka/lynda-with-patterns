using System;
using AbstractFactory.Src.Interfaces;

namespace AbstractFactory.Src.ColorClasses
{
    
    public class RedColor : AbstractColor
    {
        public override void Fill(){
            Console.WriteLine("fill red");
        }
    }

}