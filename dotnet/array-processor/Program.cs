﻿using System;

namespace array_processor
{
    class Program
    {
        /*
            B A C K L O G :
        
            - SOLID console app  Array Processor 
            - - argument processor
            - - main array processor (controller) 
            - - sort handler (implement different sort algorithms)
            - - search handler 
            - - reduce / map (apply some function to all array elements)

            - patterns (Factory, IoC, + perhaps some other),
            - services are derived from abstract service which contains argument key value 
                + some format, and process interface
            - or I can implement mapper : argument -> service pattern

            - error handler try/catch 
            - file handler read/write (regular file, json file, xml file ) 
            - number generator (randomizer)

            - data validator + arg validator
            - viewer ?/? console writeline

            - debugger/execution time


         */


        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
