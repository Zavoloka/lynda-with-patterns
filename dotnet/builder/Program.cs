﻿using System;
using Builder.Src;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            // https://www.tutorialspoint.com/design_pattern/builder_pattern.htm
            MealBuilder mealBuilder = new MealBuilder();
            Meal vegMeal = mealBuilder.PrepareVegMeal();
            Meal nonVegMeal = mealBuilder.PrepareNonVegMeal();

            Console.WriteLine("Veg meal ({0} USD) :", vegMeal.GetCost());
            vegMeal.ShowItems();
            Console.WriteLine();
            Console.WriteLine("Non Veg meal ({0} USD) :", nonVegMeal.GetCost());
            nonVegMeal.ShowItems();


        }
    }
}
