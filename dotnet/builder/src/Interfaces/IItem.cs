using System;

namespace Builder.Src.Interfaces
{
    
    public interface IItem
    {
        
        String Name();
        IPacking Packing();
        float Price();
    }

}