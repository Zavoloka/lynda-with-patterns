using System;

namespace Builder.Src.Interfaces
{
    
    public interface IPacking
    {
        string Pack();

    }

}