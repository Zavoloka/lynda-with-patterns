using System;
using Builder.Src.Interfaces;

namespace Builder.Src.ItemClasses
{
    
    public class Burger : ItemAbstract
    {
        public Burger(string name = "Regular Burger", float price = 0.99f){

            _name = name;
            _price = price;
        }


    }

}