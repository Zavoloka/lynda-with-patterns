using System;
using Builder.Src.Interfaces;

namespace Builder.Src.ItemClasses
{
    
    public class ChickenBurger : ItemAbstract
    {
        public ChickenBurger(string name = "Chicken Burger", float price = 50.0f){

            _name = name;
            _price = price;
        }


    }

}