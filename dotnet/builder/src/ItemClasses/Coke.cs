using System;

namespace Builder.Src.ItemClasses
{
    
    public class Coke : ColdDrink
    {
       
        public Coke(string name = "Coke Cold Drink", float price = 30.0f){

            _name = name;
            _price = price;
        }


    }

}