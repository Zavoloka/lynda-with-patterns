using System;
using Builder.Src.Interfaces;
using Builder.Src.PackingClasses;

namespace Builder.Src.ItemClasses
{
    
    public class ColdDrink : ItemAbstract
    {
       
        public ColdDrink(string name = "Regular Cold Drink", float price = 0.99f){

            _name = name;
            _price = price;
        }

        public override IPacking Packing(){

            return new Bottle();
        }


    }

}