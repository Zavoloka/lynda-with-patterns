using System;
using Builder.Src.Interfaces;
using Builder.Src.PackingClasses;

namespace Builder.Src.ItemClasses
{
    
    public abstract class ItemAbstract : IItem
    {
        protected string _name = "";
        protected float _price = 0.0f;

        public ItemAbstract(string name = "", float price = 0.99f){

            _name = name;
            _price = price;
        }

        public virtual string Name(){
        
            return _name;
        }

        public virtual IPacking Packing(){

            return new Wrapper();
        }

        public virtual float Price(){

            return _price;
        }

    }

}