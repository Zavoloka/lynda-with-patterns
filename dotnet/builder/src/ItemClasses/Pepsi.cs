using System;

namespace Builder.Src.ItemClasses
{
    
    public class Pepsi : ColdDrink
    {
       
        public Pepsi(string name = "Pepsi Cold Drink", float price = 35.0f){

            _name = name;
            _price = price;
        }


    }

}