using System;
using Builder.Src.Interfaces;

namespace Builder.Src.ItemClasses
{
    
    public class VegBurger : ItemAbstract
    {
        public VegBurger(string name = "Veg Burger", float price = 25.0f){

            _name = name;
            _price = price;
        }


    }

}