using System;
using System.Collections.Generic;
using Builder.Src.Interfaces;
using Builder.Src.ItemClasses;

namespace Builder.Src {

    public class Meal{

        List<IItem> _items = new List<IItem>();

        public void AddItem(IItem mealItem){

            _items.Add(mealItem);
        }   

        public float GetCost(){

            float total = 0.0f; 
            foreach(IItem item in _items){
                total += item.Price();
            }

           return total;     

        } 

        public void ShowItems(){

            uint i = 0;
            foreach (IItem item in _items){
                i++;
                string itemString = "" + item.Name() + ", " +item.Packing().Pack()
                                   + ", " + item.Price().ToString() + "$";
                Console.WriteLine("#" + i.ToString() + " " + itemString);
            }

        }


    }


}