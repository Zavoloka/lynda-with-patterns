using System;
using System.Collections.Generic;
using Builder.Src.Interfaces;
using Builder.Src.ItemClasses;

namespace Builder.Src {

    public class MealBuilder{

        public Meal PrepareVegMeal(){

            Meal meal = new Meal();
            meal.AddItem(new VegBurger());
            meal.AddItem(new Coke());

            return meal;
        }

        public Meal PrepareNonVegMeal(){

            Meal meal = new Meal();
            meal.AddItem(new ChickenBurger());
            meal.AddItem(new Pepsi());
            
            return meal;
       
        }

    }
}