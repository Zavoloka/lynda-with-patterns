using System;
using Builder.Src.Interfaces;

namespace Builder.Src.PackingClasses
{
    
    public class Bottle : IPacking
    {
        public string Pack(){

            return "Bottle pack";
        }

    }

}