using System;
using Builder.Src.Interfaces;

namespace Builder.Src.PackingClasses
{
    
    public class Wrapper : IPacking
    {
        public string Pack(){
            
            return "Wrapper pack";
        }

    }

}