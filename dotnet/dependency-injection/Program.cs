﻿using System;
using DependencyInjection.Src;
using DependencyInjection.Src.Interfaces;
using DependencyInjection.Src.MathServices;



namespace DependencyInjection
{
    
    class Program
    {
        static void Main(string[] args)
        {

            // TODO : DI MSDN https://msdn.microsoft.com/en-us/library/dd458879.aspx
            // https://softwareengineering.stackexchange.com/questions/81899/how-should-i-organize-my-source-tree
            // sourse folder structure https://msdn.microsoft.com/en-us/library/bb668992.aspx
            // https://stackoverflow.com/questions/1389458/c-sharp-project-folder-naming-conventions
            // https://msdn.microsoft.com/en-us/library/bb668992.aspx
            // https://gist.github.com/davidfowl/ed7564297c61fe9ab814
            // https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/index
            // https://www.codeproject.com/Articles/597787/A-tutorial-on-Service-locator-pattern-with-impleme

            // Naming Files, Paths, and Namespaces
            //! https://msdn.microsoft.com/en-us/library/windows/desktop/aa365247(v=vs.85).aspx
            // Proper summary
            
            // DI example
            IInterpolator MathService = new NewtonIntepolationCustomService();
            UserDataProcessor Processor1 = new UserDataProcessor(MathService);
            double[] Result = Processor1.DoActions();

            // Print result
            Console.WriteLine(string.Join(' ', Result));        
        }
    }
}
