using System;

namespace DependencyInjection.Src.Interfaces
{
    public interface IInterpolator
    {
        double[] Interpolate(double[] IncomingData);
        int[] Interpolate(int[] IncomingData);

    }
}