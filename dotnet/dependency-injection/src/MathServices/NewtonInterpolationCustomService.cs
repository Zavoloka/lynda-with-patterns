using System;
using DependencyInjection.Src.Interfaces;


namespace DependencyInjection.Src.MathServices{
    
    public class NewtonIntepolationCustomService: IInterpolator  {

        // TODO: implement interpolation
        public double[] Interpolate(double[] IncomingData){

            /* place for implementation */
            return IncomingData;
        }
        public int[] Interpolate(int[] IncomingData){

            /* place for implementation */
            return IncomingData;
        }

    }


}
// http://mathworld.wolfram.com/NewtonsForwardDifferenceFormula.html
// http://mathworld.wolfram.com/NewtonsDividedDifferenceInterpolationFormula.html