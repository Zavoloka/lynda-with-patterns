using System;
using DependencyInjection.Src.Interfaces;

namespace DependencyInjection.Src {
    public class UserDataProcessor{
        
        // heh https://stackoverflow.com/questions/12045711/is-there-any-c-sharp-naming-convention-for-a-variable-used-in-a-property
        private IInterpolator _interpolationService;
        public double[] UserRawData = {1.0, 2.0, 3.0, 4.0, 5.0};

        // Constructor Dependency Injector
        public UserDataProcessor(IInterpolator InterpolationService){

            this._interpolationService = InterpolationService;
        }

        public double[] DoActions(){

            double[] SomResult = this._interpolationService.Interpolate(this.UserRawData);
            // -----------------------------------------------
            // do some other actions, e.g. : filter or FFT
            // -----------------------------------------------
            return SomResult;

        }



    }



}