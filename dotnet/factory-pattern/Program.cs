﻿using System;
using Src;

namespace Factory
{
    class Program
    {
        static void Main(string[] args)
        {

            // TODO : check Type Design Guidelines and Naming Guidelines
            // TODO : add *.sh script to recompile and launch this app from Linux
            // TODO : https://msdn.microsoft.com/en-us/library/ee817674.aspx Project Structure
            // TODO : https://softwareengineering.stackexchange.com/questions/40394/how-do-yoau-organize-your-projects
            //        code organization
            // TODO : add additional comments
    

            // DEBUG!
            // https://docs.microsoft.com/en-us/dotnet/framework/debug-trace-profile/tracing-and-instrumenting-applications
            // https://stackoverflow.com/questions/7463377/log-a-stackoverflowexception-in-net

            // TODO : Proper commit messages https://chris.beams.io/posts/git-commit/

            SauceFactory sf = new SauceFactory();

            IFiller Sauce1 = sf.GetSauce("ketchup");
            IFiller Sauce2 = sf.GetSauce("mayonnaise");

 
            string KetchupDish1    = Sauce1.FillADish("hot dog");
            string MayonnaiseDish1 = Sauce2.FillADish("potato");
            string MayonnaiseDish2 = Sauce2.FillADish("meat");

         
            // -------------------------------------------------
            
            Console.WriteLine("Hello World!");
            Console.WriteLine("- " + KetchupDish1);
            Console.WriteLine("- " + MayonnaiseDish1);
            Console.WriteLine("- " + MayonnaiseDish2);


        }
    }
}
