using System;

namespace Src
{
    public interface IFiller
    {
        string FillADish(string DishObj);
    }
}