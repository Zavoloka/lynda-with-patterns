using System;

namespace Src
{
    class SauceFactory
    {
        public IFiller GetSauce(string SauceName){

            switch (SauceName){

                case "ketchup" : return new SauceKetchup();                          
                case "mayonnaise" : return new SauceMayonnaise();                
                default : return new SauceKetchup(); // TODO : unknown sauce case
                          
            }

        }

    }

}