using System;

namespace Src
{
    class SauceKetchup : SauceAbstract, IFiller
    {

        // TODO : add {Get/Set}
        public string Name = "Ketchup";
        protected new string[] _ReceiptList;
        private string[] ReceiptList
        {
            get {return _ReceiptList;} 
            set {_ReceiptList = value;}
        }

        public SauceKetchup(){

           this.ReceiptList = new string[]{"pasta", "potato"};          
        }
        public string FillADish(string DishObj){

            // almost anything goes :)
            string ResultDishObj = DishObj + " with " + this.Name;
            ResultDishObj = ResultDishObj + "\n" 
                          + "(but it could be much better with "
                          + string.Join(" or ", ReceiptList)
                          + ")";

            return ResultDishObj;
        }


    }
}