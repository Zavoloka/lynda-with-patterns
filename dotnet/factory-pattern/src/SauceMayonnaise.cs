using System;

namespace Src
{
    class SauceMayonnaise : SauceAbstract, IFiller
    {

        public string Name = "Mayonnaise";
        protected new string[] _ReceiptList; 
        private string[] ReceiptList{

            get {return _ReceiptList;} 
            set {_ReceiptList = value;}
        }

        public SauceMayonnaise(){

           this.ReceiptList = new string[]{"meat", "eggplant"};;
           
        }
        public string FillADish(string DishObj){

            string ResultDishObj = DishObj + " [placeholder] " + this.Name;
            bool IsReceiptExists = this.CheckReceipt(DishObj, this.ReceiptList);
            string KeyWord = (IsReceiptExists) ? "with" : "without";
            ResultDishObj = ResultDishObj.Replace("[placeholder]", newValue: KeyWord);

            return ResultDishObj;

        }



    }
}