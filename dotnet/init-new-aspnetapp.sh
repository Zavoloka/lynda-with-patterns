#!/bin/bash
APPNAME=$1

dotnet new razor -o $APPNAME

# Web related options :
# -----------------------------------------------------------------------------
#ASP.NET Core Empty                web              [C#], F#          Web/Empty          
#ASP.NET Core Web App (MVC)        mvc              [C#], F#          Web/MVC            
#ASP.NET Core Web App              razor            [C#]              Web/MVC/Razor Pages
#ASP.NET Core with Angular         angular          [C#]              Web/MVC/SPA        
#ASP.NET Core with React.js        react            [C#]              Web/MVC/SPA        
#ASP.NET Core with React.js(Redux) reactredux       [C#]              Web/MVC/SPA        
#ASP.NET Core Web API              webapi           [C#], F#   