#!/bin/bash
APPNAME=$1

dotnet new console -o $APPNAME

# -----------------------------------------------------------------------------
#Console Application        console          [C#], F#, VB      Common/Console     
#Class library              classlib         [C#], F#, VB      Common/Library     
#Unit Test Project          mstest           [C#], F#, VB      Test/MSTest        
#xUnit Test Project         xunit            [C#], F#, VB      Test/xUnit         
