﻿using System;
using System.Reflection;
using Ninject;
using NinjectIoc.Src;
using Interfaces = NinjectIoc.Src.Interfaces;
using MailServices = NinjectIoc.Src.MailServices;

namespace NinjectIoc
{
    class Program   
    {
        static void Main(string[] args)
        {

            // https://blog.agilistic.nl/a-step-by-step-guide-to-using-ninject-for-dependancy-injection-in-c-sharp/
            // http://www.ninject.org/wiki.html

            
            IKernel kernel = new StandardKernel();
            kernel.Bind<Interfaces.IMailSender>().To<MailServices.MockMailSender>();
            // get service from mapper
            var mailSender = kernel.Get<Interfaces.IMailSender>();
            Interfaces.IFormHanler formHandlerProcessor = new FormHandler(mailSender);
            formHandlerProcessor.Handle("test@test.com");
            
            // ----------------------------------------------------------------------------

            IKernel kernel2 = new StandardKernel(new NinjectBinding(isRareFlag : true));
            // get required service by FormHandler constructor
            var feedbackFormHandler = kernel2.Get<FormHandler>();
            feedbackFormHandler.Handle("test@test.com");
            
        }
    }
}
