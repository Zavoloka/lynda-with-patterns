using System;
using NinjectIoc.Src.Interfaces;
using NinjectIoc.Src.MailServices;

namespace NinjectIoc.Src
{

    public class FormHandler : IFormHanler
    {
        private readonly IMailSender mailSender;

        public FormHandler(IMailSender externalMailSender)
        {
            this.mailSender = externalMailSender;
        }

        public void Handle(string toAddress)
        {
            
            mailSender.Send(toAddress, "Ninject DI message");  
        }

    }

}