using System;

namespace NinjectIoc.Src.Interfaces
{
 public interface IMailSender
    {
        void Send(string toAddress, string subject);
    }


}

