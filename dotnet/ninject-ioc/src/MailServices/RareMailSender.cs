using System;
using NinjectIoc.Src.Interfaces;

namespace NinjectIoc.Src.MailServices
{

    public class RareMailSender : IMailSender
    {
        public void Send(string toAddress, string subject){

             Console.WriteLine("Sending rare mail to [{0}] with subject [{1}]", toAddress, subject);
        }
    }

}