using Ninject;
using Ninject.Modules;
using NinjectIoc.Src.Interfaces;
using NinjectIoc.Src.MailServices;

namespace NinjectIoc.Src
{
    public class NinjectBinding : NinjectModule
    {
        private bool _isRareLetter {get;}
        public NinjectBinding(bool isRareFlag){

            this._isRareLetter = isRareFlag;
        }
        public override void Load()
        {
            if (this._isRareLetter){
                Bind<IMailSender>().To<RareMailSender>();
                return;
            }            
            Bind<IMailSender>().To<MailSender>();
        }
    }

}