﻿using System;
using ServiceLocator.Src.Interfaces;
using ServiceLocator.Src; 

namespace ServiceLocator
{
    class Program
    {
        static void Main(string[] args)
        {   

            // https://www.codeproject.com/Articles/597787/A-tutorial-on-Service-locator-pattern-with-impleme
            // http://www.stefanoricciardi.com/2009/09/25/service-locator-pattern-in-csharpa-simple-example/
            // http://www.stefanoricciardi.com/2009/10/13/service-locator-pattern-in-c-with-lazy-initialization/
            
            //! TODO http://www.stefanoricciardi.com/2009/10/29/a-singleton-service-locator-pattern/
            // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/lock-statement
            // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/threading/index



            Console.WriteLine("Hello Service Locator example !");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
            Console.WriteLine("Int Service Locator");
            IServiceLocator serviceLocatorInstance = new IntServiceLocator();
            Int32 int32ImagineService = serviceLocatorInstance.GetService<Int32>();
            int32ImagineService = 42;
            Console.WriteLine(int32ImagineService.ToString());

            // ------------------------------------------------------

            Console.WriteLine("LazyInt Service Locator");
            IServiceLocatorUpd lazySrviceLocatorInstance = new LazyIntServiceLocator();
            Int32 lazyInt32ImagineService = serviceLocatorInstance.GetService<Int32>();
            lazyInt32ImagineService = 42 * 2;
            Console.WriteLine(lazyInt32ImagineService.ToString());

        }
    }
}
