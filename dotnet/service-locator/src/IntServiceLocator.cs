using System;
using System.Collections.Generic;
using ServiceLocator.Src.Interfaces;

namespace ServiceLocator.Src{
    public class IntServiceLocator : IServiceLocator
    {

       // mapper of available instances
       private IDictionary<object,object> _services;

       public IntServiceLocator(){

           this._services = new Dictionary<object, object>();

           this.AddServiceToMapper<Int16>();
           this.AddServiceToMapper<Int32>();
           this.AddServiceToMapper<Int64>();
        
       } 

       private void AddServiceToMapper<T>() where T : new()
       {
           this._services.Add(typeof(T), new T());
       }

       public T GetService<T>() where T : new()
       {

           if (!this._services.ContainsKey(typeof(T))){
               this.AddServiceToMapper<T>();
           }
           
           // TODO : add try/catch block
           return (T)this._services[typeof(T)];
       }




    }



}