using System;

namespace ServiceLocator.Src.Interfaces
{

    public interface IServiceLocator{
        T GetService<T>() where T : new();
    }
    
}