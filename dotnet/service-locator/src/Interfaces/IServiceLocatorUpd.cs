using System;
using System.Collections.Generic;
namespace ServiceLocator.Src.Interfaces
{

    /*
      "GetService<>()" doesn't contain unnecessary inheritance " .. where T : new();"
     */
    public interface IServiceLocatorUpd{
        T GetService<T>();
    }
    
}