using System;
using System.Reflection;
using System.Collections.Generic;
using ServiceLocator.Src.Interfaces;

namespace ServiceLocator.Src
{
    public class LazyIntServiceLocator : IServiceLocatorUpd
    {

        private List<Type> _availableServices;
        private IDictionary<Type,object> _services;


        public LazyIntServiceLocator() 
        {

            this._availableServices = new List<Type>();
            this._services = new Dictionary<Type, Object>();

            this.InitServiceMapper();

        }

        private void InitServiceMapper(){

            this._availableServices.Add(typeof(Int16));
            this._availableServices.Add(typeof(Int32));
            this._availableServices.Add(typeof(Int64));

        }

        public List<Type> GetAvailableServices(){
            return this._availableServices;
        }

        private void AddServiceToMapper<T>()
        {

            // https://msdn.microsoft.com/ru-ru/library/h93ya84h(v=vs.110).aspx     
            ConstructorInfo constructor = typeof(T).GetConstructor(new Type[0]);
            //   Debug.Assert(constructor != null, "Cannot find a suitable constructor for " + typeof(T));

            // https://msdn.microsoft.com/ru-ru/library/6ycw1y17(v=vs.110).aspx
            T service = (T)constructor.Invoke(null);
            this._services.Add(typeof(T), service);
        }


        //public getInsta
        public T GetService<T>()
        {

            // TODO : check _availableServices for this type
            // TODO : service instantiation error handler

            if (!this._services.ContainsKey(typeof(T)))
            {
               this.AddServiceToMapper<T>();    
            }

            return (T)this._services[typeof(T)];
        }



    }



}