﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SalaryCalculatorClassLibrary;

namespace SalaryCalculatorTest
{
    [TestClass]
    public class ProgramUnitTest
    {

        public int HoursInYear = 2080;

        [TestMethod]
        public void CheckAnnualySalary()
        {
            SalaryCalculatorClass scc = new SalaryCalculatorClass();
            decimal hourlyWage = 25.0m;
            decimal annualSalaryValue = scc.GetAnmualSalary(hourlyWage);

            Assert.AreEqual(hourlyWage * HoursInYear, annualSalaryValue, "Annual Salary value is not correct");

        } 


    }
}
