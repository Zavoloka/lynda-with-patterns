﻿package com.classes {
	
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.system.ImageDecodingPolicy;
	import flash.net.drm.VoucherAccessInfo;
	import flash.display.MovieClip;
	import flash.display.Stage;
	
	public class AS3Image {
		
		private const IMAGE_FILE:String = "image.jpg";
		private var imageLoader:Loader;
		private var loaderContext:LoaderContext;
		
		private var _mainMovieClip:MovieClip;

		public function AS3Image(movieClip:MovieClip) {
		
			_mainMovieClip = movieClip;
			
			// add try/catch
			loadImage();
		}
		
		private function loadImage():void {
			
			loaderContext = new LoaderContext();
			loaderContext.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD;
			
			imageLoader = new Loader();
			imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
			imageLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, imageProcess);
			imageLoader.load(new URLRequest(IMAGE_FILE), loaderContext);
			
			_mainMovieClip.addChild(imageLoader);
			
		}
		
		
		protected function imageLoaded(e:Event):void {
			
			trace("Image loaded!");
			imageLoader.height = _mainMovieClip.stage.stageHeight;
			imageLoader.scaleX = imageLoader.scaleY;
			
		}
		
		protected function imageProcess(e:ProgressEvent):void {
			
			trace(e.bytesLoaded + " / " + e.bytesTotal + " bytes loaded...");
		}

	}
	
}
