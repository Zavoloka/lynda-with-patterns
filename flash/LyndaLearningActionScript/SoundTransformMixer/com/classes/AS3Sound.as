﻿package com.classes {
	
	import flash.media.Sound;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import flash.display.MovieClip;
	
	public class AS3Sound {
		
		private const SOUND_FILE:String = "music.mp3";
		private var sound:Sound;
		private var sTransform:SoundTransform;
		private var sChannel:SoundChannel;
		private var id3:Boolean;
		private var soundBytes:ByteArray;
		
		private var _mainMovieClip:MovieClip;

		public function AS3Sound(movieClip:MovieClip) {
			// constructor code
			_mainMovieClip = movieClip;
			initStream();
		}
		
		private function initStream():void{
			
			sTransform = new SoundTransform(1, 0);
			
			sound = new Sound(new URLRequest(SOUND_FILE));
			sound.addEventListener(Event.ID3, id3Loaded);
			
			// startTime, loopsNumber
			sChannel = sound.play(0, 0, sTransform);
		}
		
		protected function id3Loaded(event:Event):void {
			
			if (!id3) {
				
				trace("Playing: "        + sound.id3.songName);
				trace("From the album: " + sound.id3.album);
				trace("By the artist: "  + sound.id3.artist);
				trace("Released in: "    + sound.id3.year);
				id3 = true;
			}
			  
			soundBytes = new ByteArray();
			_mainMovieClip.addEventListener(Event.ENTER_FRAME, monitorMixer);
		}
		
		protected function monitorMixer(e:Event):void {
			
			SoundMixer.computeSpectrum(soundBytes);
			//trace(soundBytes.bytesAvailable);
		}
		
		

	}
	
}
