﻿
package com.classes  {
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	
	import flash.events.Event;
	import flash.events.StageVideoAvailabilityEvent;
	
	//help to check if the stage video is available or not.
	import flash.events.StageVideoAvailabilityEvent; 
	
	import flash.geom.Rectangle;
	
	// stage video API
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.media.Video;
	
	// Doesn't avilable for Flash Player (only AIR)
	// includes separator and path resolvers
	//import flash.filesystem.File;
	
	// Net helpers to stream the video
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import fl.ik.ConnectionData;
	import flash.net.FileFilter;
	
	public class AS3Video extends Sprite {
		
		
		// https://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/filesystem/File.html#separator
		// https://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7fe4.html
		// We can use RegEx here
		private const DEFAULT_VIDEO_FILE_NAME:String = "video.mp4";
		private var _videoFileName:String;
		private var _video:Video;
		private var _stageVideo:StageVideo;
		
		private var _stage:Stage;
		
		// connection to video vile
		private var _nc:NetConnection;
		private var _ns:NetStream;
		private var _streamClient:Object;
		
		// ------- Constructors -----------------------------------
		public function AS3Video(externalStage:Stage, videoFile:String = null) {
			
			
			_stage = externalStage;
			
			// Init with empty object
			if (videoFile == null){
				AS3VideoWithNull();
				return;
			}
			
			// Init with file name
			AS3VideoWithFile(videoFile);
			//DEFAULT_VIDEO_FILE_NAME = "data".concat(File.separator,"video.mp4");
		}
		
		public function AS3VideoWithNull(){
						
			_videoFileName = DEFAULT_VIDEO_FILE_NAME
			initStream();
		}
		
		public function AS3VideoWithFile(videoFile:String){
			
			// TODO : check if not empty and valid
			
			_videoFileName = videoFile;
			initStream();
		}
	
		// --------------------------------------------------------
		
		
		
		
		public function onMetaData(e:Object):void {}
		
		
	
		protected function checkStageVideo(e:StageVideoAvailabilityEvent):void{
			
			if (e.availability == StageVideoAvailability.AVAILABLE && _stageVideo == null){
				buildStageVideo();
			} else {
				buildVideo();
			}
		}
		
		
		private function initStream():void {
			
			_streamClient = new Object();
			_streamClient.onMetaData = onMetaData; //only exist to satisfies requirements of the object
			
			
			_nc = new NetConnection();
			_nc.connect(null);
			

			_ns = new NetStream(_nc);
			_ns.client = _streamClient;
			
;
			_stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY, checkStageVideo);
		}
		
		
		private function buildVideo():void {
			
			_video = new Video();
			_video.attachNetStream(_ns);
			this.addChild(_video);
			_ns.play(_videoFileName);			
		}
		
		private function buildStageVideo():void {

			_stageVideo = _stage.stageVideos[0];
			_stageVideo.viewPort = new Rectangle(0, 0, _stage.stageWidth, _stage.stageHeight);
			_stageVideo.attachNetStream(_ns);
			_ns.play(_videoFileName);
			
		}
		
	
		

	}
	
}
