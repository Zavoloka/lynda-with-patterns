

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card'
import '@polymer/paper-button'
import './shared-styles.js';

class MyView4 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

        <paper-card heading="Emmental" image="http://placehold.it/350x150/FFC107/000000" alt="Emmental">
        <div class="card-content">
            Emmentaler or Emmental is a yellow, medium-hard cheese that originated in the area around Emmental, Switzerland. It is one of the cheeses of Switzerland, and is sometimes known as Swiss cheese.
        </div>
        <div class="card-actions">
            <paper-button>Share</paper-button>
            <paper-button>Explore!</paper-button>
            <paper-button>[[button3]]</paper-button>
        </div>
    </paper-card>
    `;
  }
  static get properties() {
    return {
      'button3': {
        'notify': true,
        'reflectTOAttribute': true,
        'type': String,
        'value': "TestButton3"
      }
    };
  }
}

window.customElements.define('my-view4', MyView4);
