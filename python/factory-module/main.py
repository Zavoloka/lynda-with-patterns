
from src import animals


pet = animals.AnimalFactory()

dog_pet = pet.get_animal("dog")
dog_pet.speak()

cat_pet = pet.get_animal("cat")
cat_pet.speak()
