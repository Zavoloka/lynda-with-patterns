class Animal():

    ''' Abstract animal class '''
    
    def __init__(self, name):
        self._name = name

    def speak(self):
        pass


class Cat(Animal):

    ''' Cat animal '''

    def speak(self):
        #super().speak()
        print("Meow!")


class Dog(Animal):

    ''' Dog animal '''

    def speak(self):
        #super().speak()
        print("Woof!")


class AnimalFactory():
    
    ''' Animal Factory '''

    def get_animal(self, pet):
        
        pets = dict(dog = Dog("Rocky"), cat = Cat("Tigger"))
        if pet in pets:
            return pets[pet]

        return None