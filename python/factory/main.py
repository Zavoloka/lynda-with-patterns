# Help Links

# Project structure
# https://stackoverflow.com/questions/193161/what-is-the-best-project-structure-for-a-python-application
# http://docs.python-guide.org/en/latest/writing/structure/
# https://coderwall.com/p/lt2kew/python-creating-your-project-structure
# http://www.patricksoftwareblog.com/structure-of-a-python-project/

# https://pypi.python.org/pypi/python_boilerplate_template
# https://github.com/pypa/sampleproject

# https://stackoverflow.com/questions/159720/what-is-the-naming-convention-in-python-for-variable-and-function-names
# from X import (foo, bar, blah)

# --------------------------------------------------
from src import animal_factory


pet = animal_factory.AnimalFactory()

dog_pet = pet.get_animal("dog")
dog_pet.speak()

cat_pet = pet.get_animal("cat")
cat_pet.speak()
