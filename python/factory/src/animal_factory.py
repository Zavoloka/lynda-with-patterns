from . import (dog_class, cat_class)

class AnimalFactory():
    
    ''' Animal Factory '''

    def get_animal(self, pet):
        
        pets = dict(dog = dog_class.Dog("Rocky"), cat = cat_class.Cat("Tigger"))
        if pet in pets:
            return pets[pet]

        return None

