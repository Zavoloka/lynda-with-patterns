# https://stackoverflow.com/questions/19071512/socket-error-errno-48-address-already-in-use

# search by process
ps -fA | grep python 

# seach by port
lsof -i:5000


# Sometime python process can get stuck and lock localhost:ip which raises 
# OSError: [Errno 98] Address already in use

# OR
# ConnectionRefusedError: [Errno 111] Connection refused
# from the client side 