# /usr/bin/python3

import socket

# TODO : config to extern yaml/json files
# Exception handler, e.g. : BrokenPipeError: [Errno 32] Broken pipe


def Main():
    host = '127.0.0.1'
    port = 5000

    s = socket.socket()

    # Connect to a remote socket at address.

    # The method now waits until the connection completes 
    # instead of raising an InterruptedError exception 
    # if the connection is interrupted by a signal, 
    # the signal handler doesn’t raise an exception and 
    # the socket is blocking or has a timeout 
    # (see the PEP 475 for the rationale).
    s.connect((host, port))

    message = "Test first message"
    while message != 'q':
        
        s.send(str.encode(message)) # convert to bytes
        data = s.recv(1024)
        print (">> " + str(data))
        message = input("> ")

    s.close()


if __name__ == "__main__":
    Main()