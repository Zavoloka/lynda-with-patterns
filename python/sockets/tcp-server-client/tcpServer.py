# /usr/bin/python3

import socket


# Docs: https://docs.python.org/3.6/library/socket.html
def Main():
    host = '127.0.0.1'
    port = 5000


    # Create a new socket using the given address family, 
    # socket type and protocol number. The address family should be AF_INET (the default), 
    # AF_INET6, AF_UNIX, AF_CAN or AF_RDS. The socket type 
    # should be SOCK_STREAM (the default), SOCK_DGRAM, SOCK_RAW or perhaps one
    # of the other SOCK_ constants.

    # socket.socket(family=AF_INET, type=SOCK_STREAM, proto=0, fileno=None)
    s = socket.socket()

    # attach the tuple with parameters
    # Bind the socket to address. The socket must not already be bound.
    s.bind((host, port))

    # Enable a server to accept connections. If backlog is specified, 
    # it must be at least 0 (if it is lower, it is set to 0); it specifies 
    # the number of unaccepted connections that the system will allow 
    # before refusing new connections. If not specified, a default reasonable value is chosen.

    # socket.listen([backlog])
    s.listen(1)

    print ("The server has started listening ...")

    # Accept a connection. The socket must be bound to an address and listening for connections. 
    # The return value is a pair (conn, address) where conn is a new socket object 
    # usable to send and receive data on the connection, and address is the address 
    # bound to the socket on the other end of the connection.
    c, addr = s.accept()

   
    print ("Connection from " + str(addr))

    while True:

        # Receive data from the socket. The return value is a bytes object representing 
        # the data received. The maximum amount of data to be received at once 
        # is specified by bufsize. See the Unix manual page recv(2) for the meaning 
        # of the optional argument flags; it defaults to zero.
        # For best match with hardware and network realities, the value of bufsize should be 
        # a relatively small power of 2, for example, 4096.

        # socket.recv(bufsize[, flags])
        data = c.recv(1024)
        if not data:
            break
        
        print ('Message from connected user : ' + str(data))

        # process message
        data = str(data).upper()

        print ("Sending message back : " + str(data))

        # Send data to the socket. The socket must be connected to a remote socket. 
        # The optional flags argument has the same meaning as for recv() above. 
        # Returns the number of bytes sent. Applications are responsible for 
        # checking that all data has been sent.
        c.send(str.encode(data))
    
    c.close()
    print ("The server has been halted")


# https://stackoverflow.com/questions/419163/what-does-if-name-main-do
if __name__ == "__main__":
    Main()



