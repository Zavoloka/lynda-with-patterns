Install SQL Server and create a database on Ubuntu  
https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-ubuntu  
(ctrl+shift+v to preview Markdown in VS Code)

Use Visual Studio Code to create and run Transact-SQL scripts for SQL Server   
https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-develop-use-vscode

MSSQL extension for Visual Studio Code  
https://github.com/Microsoft/vscode-mssql/wiki

- - -

1. Import the public repository GPG keys:
> curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
    
2. Register the Microsoft SQL Server Ubuntu repository:
>sudo add-apt-repository "$(curl  https://packages.microsoft.com/config/ubuntu/16.04/mssql-server-2017.list)"

3. Run the following commands to install SQL Server:
> sudo apt-get update
> sudo apt-get install mssql-server

- - -

Raw commands list :
 
> sudo /opt/mssql/bin/mssql-conf setup
>
>*(Created symlink /etc/systemd/system/multi-user.target.wants/mssql-server.service → /lib/systemd/system/mssql-server.service.)*
>
> systemctl status mssql-server

- - -

> sudo systemctl stop mssql-server  
> *(disable, enable, stop, start, etc..)*
>
> curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
>
> sudo add-apt-repository "$(curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list)"
>
> sudo apt-get update
>
> sudo apt-get install mssql-tools unixodbc-dev
>
> echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
>
> echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
>
> source ~/.bashrc
>
>
> sqlcmd -S localhost -U SA -P '<YourPassword>'
