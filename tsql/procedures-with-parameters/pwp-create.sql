USE Movies
GO

ALTER PROC [dbo].[spFilmsInYear]
    (
        @Year INT,
        @FilmName VARCHAR(MAX) OUTPUT,
        --@FilmCount INT OUTPUT,
        @Synopsis VARCHAR(MAX) OUTPUT
    )
AS
BEGIN
    SELECT
        @FilmName = FilmName,
       -- @FilmCount = COUNT(FilmID),
        @Synopsis = FilmSynopsis
    FROM
        tblFilm
    WHERE
        YEAR(FilmReleaseDate) = @Year
    ORDER BY 
        FilmName ASC   
END