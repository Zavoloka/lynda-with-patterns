USE Movies
GO

DECLARE @FilmNameStorage VARCHAR(MAX)
DECLARE @FilmCountStorage INT
DECLARE @SynopsisStorage VARCHAR(MAX)
DECLARE @YearStorage VARCHAR(4)
--SET @YearStorage = CAST('2010-01-01T00:00:00' as DATETIME('yyyy-mm-ddThh:mi:ss.mmm'))
SET @YearStorage = 2000

-- Launch the procedure
EXEC spFilmsInYear
      @Year = @YearStorage, 
      @FilmName  = @FilmNameStorage OUTPUT,
   --   @FilmCount = @FilmCountStorage OUTPUT,
      @Synopsis  = @SynopsisStorage  OUTPUT


-- Print the data 
PRINT N'Films year : ' + @YearStorage
--PRINT "Films count : " + @FilmCountStorage
SELECT 
      @FilmNameStorage as [Name], 
      @SynopsisStorage as [Synopsis]

