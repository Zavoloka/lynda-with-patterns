USE Movies
GO -- begins a new batch

ALTER PROC spFilmList
AS
BEGIN
    SELECT
        TOP 25
        FilmName,
        FilmReleaseDate,
        FilmRunTimeMinutes
    FROM
        tblFilm
    ORDER BY
        FilmReleaseDate ASC
END