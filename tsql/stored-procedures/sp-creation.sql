-- uses DB from qf-898.zip 
-- https://www.youtube.com/playlist?list=PLNIs-AWhQzcleQWADpUgriRxebMkMmi4H
--
-- WiseOWL (SQL Server - Procedures and Programming)
-- CTRL+SHIFT+E to execute script (VS Code)

USE Movies
GO -- begins a new batch

CREATE PROC spFilmList
AS
BEGIN
    SELECT
        FilmName,
        FilmReleaseDate,
        FilmRunTimeMinutes
    FROM
        tblFilm
    ORDER BY
        FilmName ASC
END